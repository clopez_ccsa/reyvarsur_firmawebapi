﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Models
{
    public class DocumentSignatureSendModel : BaseModel
    {
        public int docType { get; set; }
        public string docEntry { get; set; }
        public string userCode { get; set; }
        public bool sendByMail { get; set; }
        public byte[] signature { get; set; }

        //Datos del firmante
        public string firmaNombre { get; set; }
        public string firmaApellidos { get; set; }
        public string firmaNIF { get; set; }
        public string firmaMatricula { get; set; }
    }
}