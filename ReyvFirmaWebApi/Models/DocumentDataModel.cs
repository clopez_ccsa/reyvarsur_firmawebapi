﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ReyvFirmaWebApi.Models
{
    public class DocumentDataModel : BaseModel
    {
        public int docType { get; set; }
        public string docNum { get; set; }
        public string cardCode { get; set; }
        public string cardName { get; set; }
        public DateTime docDate { get; set; }
        public int nItems { get; set; }
    }

    public class DocumentLineDataModel : BaseModel
    {
        //ID, Name
        public double quantity { get; set; }
    }

    #region Specific document data *****************************
    //Albarán de entrega (Delivery)
    public class DeliveryDataModel : DocumentDataModel
    {
        public double docTotal { get; set; }
    }

    //Traspasos
    public class InventoryTransferDataModel : DocumentDataModel
    {
        public string almacenOrigID { get; set; }
        public string almacenOrig { get; set; }
        public string almacenDestID { get; set; }
        public string almacenDest { get; set; }
    }

    //Control de visitas
    public class VisitorDataModel : DocumentDataModel
    {
        public string visitorId { get; set; }
        public string visitorName { get; set; }
        public string inChargeId { get; set; }
        public string inChargeName { get; set; }
        public string dni { get; set; }
        public DateTime timeIn { get; set; }
        public DateTime timeOut { get; set; }
    }

    //Entrega de EPIS
    public class EpisDataModel : DocumentDataModel
    {
        public List<DocumentLineDataModel> itemList { get; set; }
    }
    #endregion ************************************************
}
