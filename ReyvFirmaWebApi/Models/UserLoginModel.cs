﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Models
{
    public class UserLoginModel
    {
        [Required]
        public string userName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string password { get; set; }
        [Required]
        public string token { get; set; }

        public string userFirstName { get; set; }
        public string userLastName { get; set; }
        public int idEmpleadoVentas { get; set; }

        public UserLoginModel (string _userName, string _password, string _token)
        {
            userName = _userName;
            password = _password;
            token = _token;
        }

        public UserLoginModel(string _userName, string _password, string _token,
                              string _userFirstName, string _userLastName, int _idEmpleadoVentas)
        {
            userName = _userName;
            password = _password;
            token = _token;
            userFirstName = _userFirstName;
            userLastName = _userLastName;
            idEmpleadoVentas = _idEmpleadoVentas;
        }
    }
}