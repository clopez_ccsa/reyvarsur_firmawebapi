﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Models
{
    public class DocumentTypeModel
    {
        public enum DocumentTypeEnum { Ninguno, Albaran, Traslados, Visitas, Epis, Envios, Vacaciones }
        public int DocumentType { get; set; }
        public string IconActive { get; set; }
        public string IconInactive { get; set; }
    }
}