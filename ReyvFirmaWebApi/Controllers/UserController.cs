﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ReyvFirmaWebApi.Models;
using ReyvFirmaWebApi.Objects;
using System.Web;
using ReyvFirmaWebApi.Objects.SAP;

namespace ReyvFirmaWebApi.Controllers
{
    [Filters.BasicAuthentication]
    public class UserController : ApiController
    {

        private UserLoginModel userLogin
        {
            get { return (UserLoginModel)Request.Properties["WebAPILoginInfo"]; }
        }

        
        /// <summary>
        /// Get the user name and password and check if it exists on SAP (via the single connection + DOIUSR table,
        /// or it's own SAP's username+password. The data comes from the AUTH field of the HTML, using Basic
        /// Authorization level
        /// </summary>
        /// <returns>An HTTP response message</returns>
        // POST: api/User
        [HttpPost]
        public HttpResponseMessage Login()
        {
            UserConnection newUser;
            HttpResponseMessage message;
            int connectionTry;
            
            ErrorHandler.WriteInLog("****** CONNECTING: " + userLogin.userName + ":" + userLogin.password.Substring(0,1) +
                                    "**" + userLogin.password.Substring(userLogin.password.Length - 1),
                                    ErrorHandler.ErrorVerboseLevel.Trace);

            try
            {

                if (!String.IsNullOrWhiteSpace(userLogin.password))
                {
                    connectionTry = ConnectionsManager.TryToConnectUser(userLogin.userName, userLogin.password, out newUser);
                    switch (connectionTry)
                    {
                        case 1:
                            userLogin.token = newUser.token;
                            ErrorHandler.WriteInLog("User '" + userLogin.userName + "' connected to the web api.", ErrorHandler.ErrorVerboseLevel.Trace);

                            try
                            {
                                //Now getting extra information about the logged user
                                switch (GetExtraInformation())
                                {
                                    case 0:
                                        message = Request.CreateResponse(HttpStatusCode.OK, userLogin, Configuration.Formatters.JsonFormatter);
                                        break;
                                    case 2:
                                        message = Request.CreateResponse(HttpStatusCode.Forbidden, "Access denied.");
                                        ErrorHandler.WriteInLog("Access denied for user " + userLogin.userName + " trying to get aditional info.",
                                                                ErrorHandler.ErrorVerboseLevel.OnlyError);
                                        break;
                                    default:
                                        message = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error connecting to SAP. Ask to your administrator");
                                        ErrorHandler.WriteInLog("Unable to access SAP for retreiving more user info.", ErrorHandler.ErrorVerboseLevel.None);
                                        break;
                                }
                                
                            }
                            catch (Exception ex)
                            {
                                message = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
                                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                            }
                            break;
                        case 0:
                            message = Request.CreateResponse(HttpStatusCode.Forbidden, "Access denied.");
                            ErrorHandler.WriteInLog("Access denied for user " + userLogin.userName, ErrorHandler.ErrorVerboseLevel.OnlyError);
                            break;
                        case -1:
                            message = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Error connecting to SAP. Ask to your administrator");
                            ErrorHandler.WriteInLog("User connection failure. Unable to connect to SAP", ErrorHandler.ErrorVerboseLevel.None);
                            break;
                        default:
                            message = Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Unknown error!!!");
                            ErrorHandler.WriteInLog("TryToConnectUser has return  a not valid value", ErrorHandler.ErrorVerboseLevel.OnlyError);
                            break;
                    }
                }
                else
                {
                    message = Request.CreateResponse(HttpStatusCode.Forbidden, "Access denied!! Blank password.");
                    ErrorHandler.WriteInLog("Access denied for blank password", ErrorHandler.ErrorVerboseLevel.OnlyError);
                }
            }
            catch (Exception ex)
            {
                message = Request.CreateResponse(HttpStatusCode.InternalServerError, "Error connecting to SAP. Ask to your administrator");
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }

            return message;
        }

        private int GetExtraInformation()
        {
            string _firstName;
            string _lastName;
            int tempInt;
            int _error;

            _error = SAPUserData.GetFirstLastUserName(userLogin.token, userLogin.userName, out _firstName, out _lastName);
            userLogin.userFirstName = _firstName;
            userLogin.userLastName = _lastName;

            SAPSalesPerson.GetSalesId(userLogin.token, userLogin.userName, out tempInt);
            userLogin.idEmpleadoVentas = tempInt;

            return _error;
        }

        /// <summary>
        /// Check if user is still connected to the DataBase, using token to identify
        /// </summary>
        /// <returns>200 = OK, 401 = not logged or timed out</returns>
        [HttpPost]
        public HttpResponseMessage Check([FromBody] UserLoginModel bodyLogin)
        {
            HttpResponseMessage message;
            ErrorHandler.WriteInLog("****** COMPROBANDO USER: " + bodyLogin.token, ErrorHandler.ErrorVerboseLevel.None);

            if (!String.IsNullOrWhiteSpace(bodyLogin.token))
            {
                if (ConnectionsManager.FindUserConnectionByToken(bodyLogin.token) != null)
                {
                    message = Request.CreateResponse(HttpStatusCode.OK);
                }
                else
                {
                    message = Request.CreateResponse(HttpStatusCode.Unauthorized, "User not logged or timed out!");
                    ErrorHandler.WriteInLog("User not logged or timed out!", ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            else
            {
                message = Request.CreateResponse(HttpStatusCode.Unauthorized, "User not logged or timed out!");
                ErrorHandler.WriteInLog("User not logged or timed out!", ErrorHandler.ErrorVerboseLevel.None);
            }

            return message;
        }
    }
}
