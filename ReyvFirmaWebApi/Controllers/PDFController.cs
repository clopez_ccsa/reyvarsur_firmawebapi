﻿using ReyvFirmaWebApi.Objects;
using ReyvFirmaWebApi.Objects.SAP.SAPPDF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ReyvFirmaWebApi.Controllers
{
    [Filters.TokenAuthentication]
    public class PDFController : ApiController
    {
        [Route("api/documents/securitySheet")]
        public HttpResponseMessage GetSecuritySheetURL()
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string documentURL = null;

            HttpStatusCode statusCode = ControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Get Security Sheet URL.", ErrorHandler.ErrorVerboseLevel.Trace);
                documentURL = SAPPDFData.GetSecuritySheetURL(oCompany);
                if (!string.IsNullOrWhiteSpace(documentURL))
                    message = Request.CreateResponse(HttpStatusCode.OK, documentURL, System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                else
                    message = Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/documents/lopd")]
        public HttpResponseMessage GetLOPDURL()
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string documentURL = null;

            HttpStatusCode statusCode = ControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Get LOPD URL.", ErrorHandler.ErrorVerboseLevel.Trace);
                documentURL = SAPPDFData.GetLOPDURL(oCompany);
                if (!string.IsNullOrWhiteSpace(documentURL))
                    message = Request.CreateResponse(HttpStatusCode.OK, documentURL, System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
                else
                    message = Request.CreateResponse(HttpStatusCode.OK);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
    }
}
