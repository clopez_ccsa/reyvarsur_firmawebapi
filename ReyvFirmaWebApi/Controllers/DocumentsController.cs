﻿using ReyvFirmaWebApi.Models;
using ReyvFirmaWebApi.Objects;
using ReyvFirmaWebApi.Objects.SAP;
using ReyvFirmaWebApi.Objects.SAP.SAPSignatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ReyvFirmaWebApi.Controllers
{
    [Filters.TokenAuthentication]
    public class DocumentsController : ApiController
    {
        [Route("api/documents/list")]
        public HttpResponseMessage GetDocuments([FromUri] string userCode, int docType)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<DocumentDataModel> documentList = null;

            HttpStatusCode statusCode = ControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                DocumentTypeModel.DocumentTypeEnum docTypeEnum = (DocumentTypeModel.DocumentTypeEnum)Enum.ToObject(typeof(DocumentTypeModel.DocumentTypeEnum), docType);
                ErrorHandler.WriteInLog(string.Format("* Get documents for salesPerson({0}): type({1}) = {2}",
                                                      userCode, docType, Enum.GetName(typeof(DocumentTypeModel.DocumentTypeEnum), docType)), ErrorHandler.ErrorVerboseLevel.Trace);
                documentList = SAPSignDocuments.GetDocumentToSignList(oCompany, userCode, docType);
                message = Request.CreateResponse(HttpStatusCode.OK, documentList, System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }

        [Route("api/documents/lines")]
        public HttpResponseMessage GetLines([FromUri] string docEntry, int docType)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            List<DocumentLineDataModel> documentLines = null;

            HttpStatusCode statusCode = ControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                DocumentTypeModel.DocumentTypeEnum docTypeEnum = (DocumentTypeModel.DocumentTypeEnum)Enum.ToObject(typeof(DocumentTypeModel.DocumentTypeEnum), docType);
                ErrorHandler.WriteInLog(string.Format("* Get lines for document({0}): type({1}) = {2}",
                                                      docEntry, docType, Enum.GetName(typeof(DocumentTypeModel.DocumentTypeEnum), docType)), ErrorHandler.ErrorVerboseLevel.Trace);
                documentLines = SAPSignDocuments.GetDocumentLines(oCompany, docEntry, docType);
                message = Request.CreateResponse(HttpStatusCode.OK, documentLines, System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }


        [Route("api/documents/sign")]
        public HttpResponseMessage PostSignature([FromBody] DocumentSignatureSendModel signature)
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string _error;

            HttpStatusCode statusCode = ControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog(string.Format("* Add signature: Doc type={0}, DocEntry={1}, UserCode={2}", signature.docType, signature.docEntry, signature.userCode),
                                        ErrorHandler.ErrorVerboseLevel.Trace);
                _error = SignatureManager.AddSignature(oCompany, signature);

                if (string.IsNullOrWhiteSpace(_error))
                {
                    ErrorHandler.WriteInLog(string.Format("  Send mail: {0}", signature.sendByMail.ToString()), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (signature.sendByMail)
                        _error = SAPSignaturesMail.SendMailForDocument(oCompany, signature);
                }

                if (string.IsNullOrWhiteSpace(_error))
                    message = Request.CreateResponse(HttpStatusCode.OK);
                else
                    message = Request.CreateResponse(HttpStatusCode.OK, _error,
                                System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
    }
}
