﻿using ReyvFirmaWebApi.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;

namespace ReyvFirmaWebApi.Controllers
{
    public class ControllersCommonFunctions
    {
        public static HttpStatusCode CheckToken(HttpRequestMessage Request, out SAPbobsCOM.Company oCompany)
        {
            HttpStatusCode statusCode;
            IEnumerable<string> values;
            string _token = "";
            oCompany = null;

            Request.Headers.TryGetValues("Token", out values);
            _token = values.First<string>();

            if (!string.IsNullOrWhiteSpace(_token))
            {
                oCompany = ConnectionsManager.GetCompanyByToken(_token, true);
                if (oCompany != null)
                {
                    statusCode = HttpStatusCode.OK;
                }
                else
                {
                    statusCode = HttpStatusCode.RequestTimeout;
                }
            }
            else
            {
                //Ya se verificó el Token en TokenAuthentificationAttribute. Si no es válido, algo ha pasado
                //en el código entre medias
                statusCode = HttpStatusCode.InternalServerError;
            }

            return statusCode;
        }
    }
}