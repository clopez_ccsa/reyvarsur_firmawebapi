﻿using ReyvFirmaWebApi.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ReyvFirmaWebApi.Controllers
{
    [Filters.TokenAuthentication]
    public class GetTextController : ApiController
    {
        [Route("api/text/lopd")]
        public HttpResponseMessage GetTextLOPD()
        {
            SAPbobsCOM.Company oCompany;
            HttpResponseMessage message;
            string dbText = null;

            HttpStatusCode statusCode = ControllersCommonFunctions.CheckToken(Request, out oCompany);
            if (statusCode == HttpStatusCode.OK)
            {
                ErrorHandler.WriteInLog("* Get LOPD text.", ErrorHandler.ErrorVerboseLevel.Trace);
                dbText = SAPBDGenerics.GetFirstValueFormTable(oCompany, "@DOI_CONF_VISITAS", "U_DOI_LOPD", "\"Code\" = '00000001'");
                if (string.IsNullOrEmpty(dbText))
                    ErrorHandler.WriteInLog(string.Format("* No se ha podido cargar el campo {0} de la tabla {1} para el id {2}.", "@DOI_CONF_VISITAS", "U_DOI_LOPD", "'00000001'"),
                                                          ErrorHandler.ErrorVerboseLevel.Trace);
                message = Request.CreateResponse(HttpStatusCode.OK, dbText, System.Net.Http.Formatting.JsonMediaTypeFormatter.DefaultMediaType);
            }
            else
            {
                message = Request.CreateResponse(statusCode);
            }

            return message;
        }
    }
}
