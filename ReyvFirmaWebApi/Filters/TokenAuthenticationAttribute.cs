﻿using ReyvFirmaWebApi.Models;
using ReyvFirmaWebApi.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace ReyvFirmaWebApi.Filters
{
    public class TokenAuthenticationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            try
            {
                string _token = "";
                IEnumerable<string> values;
                actionContext.Request.Headers.TryGetValues("Token", out values);
                _token = values.First<string>();

                if (!string.IsNullOrWhiteSpace(_token))
                    if (ConnectionsManager.FindUserConnectionByToken(_token) == null)
                        throw new HttpResponseException(System.Net.HttpStatusCode.Forbidden);
                    

            } catch (Exception ex)
            {
                actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                ErrorHandler.WriteInLog("EXCEPTION on authorization: " + ex, ErrorHandler.ErrorVerboseLevel.Trace);
            }
            
        }
    }
}