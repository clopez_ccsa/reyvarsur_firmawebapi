﻿using ReyvFirmaWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace ReyvFirmaWebApi.Filters
{
    public class BasicAuthenticationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            UserLoginModel _userLogin;
            System.Diagnostics.Debug.WriteLine("Filtro de autentificación activo");

            if (actionContext.Request.Headers.Authorization == null)
                actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            else
            {
                //Ojo, este token no tiene nada que ver con el token de usuario, es una cadena que codifica
                //el usuario y la contraseña de la cabecera "Basic Auth"
                string authToken = actionContext.Request.Headers.Authorization.Parameter;
                string decodedToken;
                string decodedUser = "";
                string decodedPassword = "";
                if (!String.IsNullOrWhiteSpace(authToken))
                {
                    decodedToken = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(authToken));
                    decodedUser = decodedToken.Substring(0, decodedToken.IndexOf(":"));
                    decodedPassword = decodedToken.Substring(decodedToken.IndexOf(":") + 1);
                }
                _userLogin = new UserLoginModel(decodedUser, decodedPassword, "");
                actionContext.Request.Properties["WebAPILoginInfo"] = _userLogin;
            }

            //base.OnActionExecuting(actionContext);
        }
    }
}