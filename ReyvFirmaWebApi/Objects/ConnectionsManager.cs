﻿using ReyvFirmaWebApi.Objects.SAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Objects
{
    public static class ConnectionsManager
    {

        public enum ConnectionMode { _UserConnection, _SharedConnection };
        public static ConnectionMode connectionMode = CheckConnectionType();
        public const string sharedGroupName = "shared";


        /// <summary>
        /// List of connections, saved as a pair of 'name of group'-'CompanyConection object'
        /// </summary>
        static Dictionary<string, CompanyConnection> companyConnectionList =
            new Dictionary<string, CompanyConnection>();

        /// <summary>
        /// Listof users conected to the application
        /// </summary>
        static List<UserConnection> userConnectionList = new List<UserConnection>();


        #region misc methods ******************************************************************************************

        private static ConnectionMode CheckConnectionType ()
        {
            ConnectionMode connectionReturn;
            string tempString = System.Configuration.ConfigurationManager.AppSettings.Get("SharedConnectionMode");
            if (tempString.ToLower() == "true")
                connectionReturn = ConnectionMode._SharedConnection;
            else
                connectionReturn = ConnectionMode._UserConnection;

            return connectionReturn;
        }

        #endregion ****************************************************************************************************

        #region company connection list management ********************************************************************

        /// <summary>
        /// Adds a connection with the _groupName as a key, in the list. If the connection already exists, it
        /// replaces the old one with the new, so UserConnections of the same group should update it
        /// </summary>
        /// <param name="_groupName"></param>
        /// <param name="_connection"></param>
        public static void AddConnection (string _groupName, CompanyConnection _connection)
        {
            if (companyConnectionList.ContainsKey(_groupName))
            {
                //If there is already a connection for this group, we must delete it.
                //For that, if a conection is disconnected, the users will stop using it,
                //so we only need to stop referencing it on this list
                companyConnectionList[_groupName].Disconnect();
                companyConnectionList.Remove(_groupName);
            }
            companyConnectionList.Add(_groupName, _connection);
        }

        
        public static CompanyConnection GetConnection (string _group, bool updateConection)
        {
            CompanyConnection companyConnectionToReturn = null;
            if (companyConnectionList.ContainsKey(_group))
            {
                companyConnectionToReturn = companyConnectionList[_group];
                if (updateConection)
                    companyConnectionToReturn.GetCompany(true);
            }
            return companyConnectionToReturn;
        }

        

        private static int CountCompanyConnection (string groupConnection)
        {
            int contConnections = 0;
            
            foreach (UserConnection _elem in userConnectionList)
            {
                if (_elem.connectionGroup == groupConnection)
                    contConnections++;
            }

            return contConnections;
        }

        #endregion ****************************************************************************************************

        #region user connection list management ***********************************************************************
        /// <summary>
        /// It's adds an user to the list of users if it exists on SAP. It's called only on login
        /// screen of the app.
        /// If the user already exists in the list and is a valid one...
        /// </summary>
        /// <param name="_user">Class with user info, where only user and passwor are mandatory.</param>
        /// <returns></returns>
        public static void AddUser (UserConnection _user)
        {
            bool _found = false;
            
            //First, look if user exists on list (has to have the same user name and token
            _found = FindUser(_user);
            //If exists, delete it!
            if (_found)
            {
                DeleteUser(_user.username, _user.token);
            }
            userConnectionList.Add(_user);
            _user.AddToTimeoutEvents();
        }

        private static bool FindUser (UserConnection _user)
        {
            bool _found = false;
            int cont = 0;

            while (!_found && (cont < userConnectionList.Count))
            {
                if ((userConnectionList[cont].username == _user.username) &&
                    (userConnectionList[cont].token == _user.token))
                {
                    _found = true;
                }
                cont++;
            }

            return _found;
        }

        public static void DeleteUser (UserConnection _user)
        {
            //Hay que comprobar si hay más usuarios compartiendo su conexión a SAP, de lo
            //contrario deberíamos cerrar también dicha conexión
            ErrorHandler.WriteInLog("Esta conexión tiene los siguientes usuarios: " +
                                                CountCompanyConnection(_user.connectionGroup),
                                                ErrorHandler.ErrorVerboseLevel.OnlyError);
            if (_user.connectionGroup != null)
            {
                if (CountCompanyConnection(_user.connectionGroup) == 1)
                {
                    GetConnection(_user.connectionGroup, false).Disconnect();
                    companyConnectionList.Remove(_user.connectionGroup);
                }
            }

            if (userConnectionList.Contains(_user))
            {
                _user.DeleteFromTimeoutEvents();
                userConnectionList.Remove(_user);
            }

            ErrorHandler.WriteInLog("Número de usuarios actualmente: " + userConnectionList.Count().ToString(),
                                    ErrorHandler.ErrorVerboseLevel.OnlyError);
        }

        private static void DeleteUser (string _user, string _token)
        {
            if (!String.IsNullOrWhiteSpace(_user) && !String.IsNullOrWhiteSpace(_token))
            {
                bool _found = false;
                int cont = 0;

                while (!_found && (cont < userConnectionList.Count))
                {
                    if ((userConnectionList[cont].username == _user) &&
                        (userConnectionList[cont].token == _token))
                    {
                        userConnectionList[cont].DeleteFromTimeoutEvents();
                        userConnectionList.RemoveAt(cont);
                        _found = true;
                    }
                    cont++;
                }
            }
        }

        #endregion ****************************************************************************************************


        /// <summary>
        /// This method tries to connect an user to SAP using his/her user and password.
        /// WARNING! This method will kick a previous connection of this user
        /// </summary>
        /// <param name="_user"></param>
        /// <param name="_password"></param>
        /// <param name="newUser"></param>
        /// <returns>-1 connection error, 0 = Not registered, 1 = Registered</returns>
        public static int TryToConnectUser (string _user, string _password, out UserConnection newUser)
        {
            int _allowed = 0;
            int errorCode = 0;
            string connectionGroup;
            CompanyConnection companyConnection = null;
            string newToken;

            newUser = null;
            newToken = HttpServerUtility.UrlTokenEncode(System.Text.Encoding.ASCII.GetBytes(_user + _password));
            ErrorHandler.WriteInLog("Searching Company connection for user " + _user + " (" + newToken + ")",
                                    ErrorHandler.ErrorVerboseLevel.Trace);
            companyConnection = GetCompanyConnectionByToken(newToken);

            if (companyConnection != null)
            {
                ErrorHandler.WriteInLog("Company connection for " + newToken + " found. Looking for his User Connection",
                                        ErrorHandler.ErrorVerboseLevel.Trace);
                _allowed = 1;
                newUser = FindUserConnectionByToken(newToken);

                if (newUser != null)
                    ErrorHandler.WriteInLog("User connection group: " + newUser.connectionGroup, ErrorHandler.ErrorVerboseLevel.OnlyError);
                else
                    ErrorHandler.WriteInLog("WARNING! User connection not found!", ErrorHandler.ErrorVerboseLevel.OnlyError);
            }
            else
            {
                ErrorHandler.WriteInLog("Company connection for " + newToken + " not found. Checking user with SAP.",
                                        ErrorHandler.ErrorVerboseLevel.OnlyError);
                _allowed = SAPConnections.CheckUserOnSAP(_user, _password, out companyConnection);


                //Exception: if user exists but doesn't have a SAP license, it only can connect if it's a shared connection
                if (_allowed == 2)
                    if (connectionMode == ConnectionMode._SharedConnection)
                        _allowed = 1;
                    else
                        _allowed = 0;

                ErrorHandler.WriteInLog("Conexiones abiertas: " + companyConnectionList.Count, ErrorHandler.ErrorVerboseLevel.Trace);
                ErrorHandler.WriteInLog(companyConnectionList.Keys.ToList<string>(), ErrorHandler.ErrorVerboseLevel.Trace);

                //If SAP user login has been successful, we look for the connection or we create it and add it to the list
                if (_allowed == 1)
                {

                    ErrorHandler.WriteInLog(string.Format("User allowed! Connection mode {0}", connectionMode.ToString()), ErrorHandler.ErrorVerboseLevel.Trace);

                    if (connectionMode == ConnectionMode._SharedConnection)
                    {
                        //Now we must disconnect the user and reconnect with the shared user
                        if ((companyConnection != null) && (companyConnection.IsConnected()))
                        {
                            companyConnection.Disconnect();
                            companyConnection = null;
                        }

                        //looking for an already opened connection
                        ErrorHandler.WriteInLog(string.Format("Getting Company Connection for shared group {0}", sharedGroupName),
                                                ErrorHandler.ErrorVerboseLevel.Trace);
                        companyConnection = GetConnection(sharedGroupName, true);

                        ErrorHandler.WriteInLog("Conexiones abiertas: " + companyConnectionList.Count, ErrorHandler.ErrorVerboseLevel.Trace);
                        ErrorHandler.WriteInLog(companyConnectionList.Keys.ToList<string>(), ErrorHandler.ErrorVerboseLevel.Trace);

                        if (companyConnection == null)  //if not, we need to create a new one
                        {
                            ErrorHandler.WriteInLog("Making a shared connection", ErrorHandler.ErrorVerboseLevel.ErrorAndData);
                            //TODO: Cambiar las strings de user y pwd por datos de otra fuente
                            errorCode = SAPConnections.MakeConnection(
                                                        System.Configuration.ConfigurationManager.AppSettings.Get("SharedUser"),
                                                        SAPDesglose.Decrypt(System.Configuration.ConfigurationManager.AppSettings.Get("SharedPw")),
                                                        out companyConnection);
                            if (errorCode == 0)
                            {
                                companyConnection.AddToTimeoutEvents();
                                AddConnection(sharedGroupName, companyConnection);
                            }
                        }
                        ErrorHandler.WriteInLog("Connecting shared group connection", ErrorHandler.ErrorVerboseLevel.OnlyError);
                        if (!companyConnection.IsConnected())
                            companyConnection.Connect();

                        connectionGroup = sharedGroupName;
                    }
                    else
                    {
                        //If connection is not shared
                        connectionGroup = _user;
                        //looking for an already opened connection
                        ErrorHandler.WriteInLog("Checking for an already opened connection", ErrorHandler.ErrorVerboseLevel.Trace);
                        if (companyConnectionList.ContainsKey(_user))
                        {
                            ErrorHandler.WriteInLog(string.Format("Connection found for {0}. Discarding new connection", _user),
                                                    ErrorHandler.ErrorVerboseLevel.Trace);
                            //if it exists, we must discard the new one
                            companyConnection.Disconnect();
                            companyConnectionList.Remove(_user);
                            companyConnection = null;
                            companyConnection = GetConnection(_user, false);

                            if (!companyConnection.IsConnected())
                                companyConnection.Connect();
                        }
                        else
                        {
                            ErrorHandler.WriteInLog("Adding new Company connection to the list", ErrorHandler.ErrorVerboseLevel.Trace);
                            //If not, we add it to the list
                            companyConnection.AddToTimeoutEvents();
                            AddConnection(connectionGroup, companyConnection);
                        }
                    }

                    ErrorHandler.WriteInLog("Adding new User connection to the list", ErrorHandler.ErrorVerboseLevel.Trace);
                    newUser = new UserConnection(_user, _password, newToken);
                    newUser.connectionGroup = connectionGroup;
                    AddUser(newUser);
                }
            }

            return _allowed;
        }

        #region get SAP connection by token **************************************************************************
        /// <summary>
        /// Returns connection data of an user. It doesn't connect or anything, just give you information.
        /// Check if you really want to use GetCompanyByToken instead to get user connection to SAP
        /// </summary>
        /// <param name="_token"></param>
        /// <returns></returns>
        public static UserConnection FindUserConnectionByToken(string _token)
        {
            UserConnection userConnection = null;
            int cont = 0;

            ErrorHandler.WriteInLog("FindUserConnectionByToken (" + _token + ")", ErrorHandler.ErrorVerboseLevel.Trace);
            while ((cont < userConnectionList.Count()) && (userConnection == null))
            {
                if (userConnectionList[cont].token == _token)
                    userConnection = userConnectionList[cont];
                cont++;
            }

            return userConnection;
        }
        
        


        /// <summary>
        /// Returns the CompanyConnection of a given user (by token). It looks for the connection on the list using
        /// token as reference
        /// </summary>
        /// <param name="_token"></param>
        /// <param name="_updateDate"></param>
        /// <returns></returns>
        public static CompanyConnection GetCompanyConnectionByToken (string _token)
        {
            UserConnection userConnection;
            CompanyConnection companyConnection = null;

            if (!string.IsNullOrWhiteSpace(_token))
            {
                ErrorHandler.WriteInLog("Finding user connection by token", ErrorHandler.ErrorVerboseLevel.Trace);
                userConnection = FindUserConnectionByToken(_token);

                if (userConnection != null)
                {
                    ErrorHandler.WriteInLog("User connection found. Looking for Company connection, group " + userConnection.connectionGroup,
                                            ErrorHandler.ErrorVerboseLevel.Trace);

                    //If there is no conected user with this name, user must put user and password again
                    //Looking if the conection of this user exists
                    if (!String.IsNullOrWhiteSpace(userConnection.connectionGroup) &&
                        (companyConnectionList.ContainsKey(userConnection.connectionGroup)))
                    {
                        companyConnection = GetConnection(userConnection.connectionGroup, false);

                        if (companyConnection != null)
                            ErrorHandler.WriteInLog("Company connection found!", ErrorHandler.ErrorVerboseLevel.Trace);
                    }

                    //if userConection has companyConection as null, or it's oCompany as null, or it's
                    //conection has expired (is not on the companyConectionList), we'll try to conect
                    //again
                    if ((companyConnection == null) || (companyConnection.GetCompany(false) == null) ||
                        !companyConnection.GetCompany(false).Connected)
                    {
                        ErrorHandler.WriteInLog("Company connection NOT found or not connected!",
                                                ErrorHandler.ErrorVerboseLevel.OnlyError);
                    }
                } else
                {
                    ErrorHandler.WriteInLog("User connection not found for token " + _token, ErrorHandler.ErrorVerboseLevel.ErrorAndData);
                }
            }

            return companyConnection;
        }


        /// <summary>
        /// Returns the oCompany of a given user (by token). It looks for the connection on the list using
        /// token as reference and can try to reconnect under specifics circumstances
        /// </summary>
        /// <param name="_token">The user connection identifier token</param>
        /// <param name="_updateDate">True if you want to reset the connection time counter</param>
        /// <returns></returns>
        public static SAPbobsCOM.Company GetCompanyByToken(string _token, bool _updateDate)
        {
            SAPbobsCOM.Company oCompany = null;
            CompanyConnection companyConnection = GetCompanyConnectionByToken(_token);
            UserConnection uConnection;

            ErrorHandler.WriteInLog("GetCompanyByToken (" + _token + ")", ErrorHandler.ErrorVerboseLevel.Trace);

            //Updating user connection timeout if necessary
            if (_updateDate)
            {
                ErrorHandler.WriteInLog("Updating timeout for UserConnection.", ErrorHandler.ErrorVerboseLevel.Trace);
                uConnection = FindUserConnectionByToken(_token);
                if (uConnection != null)
                    uConnection.UpdateDateTime();
            }

            if (companyConnection != null)
            {
                if (!companyConnection.IsConnected())
                    companyConnection.Connect();
                oCompany = companyConnection.GetCompany(_updateDate);
            }

            return oCompany;
        }

        #endregion ********************************************************************************************************


    }
}