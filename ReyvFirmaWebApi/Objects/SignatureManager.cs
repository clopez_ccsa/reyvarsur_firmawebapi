﻿using ReyvFirmaWebApi.Models;
using ReyvFirmaWebApi.Objects.SAP.SAPAttachments;
using ReyvFirmaWebApi.Objects.SAP.SAPSignatures;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ReyvFirmaWebApi.Objects
{
    public static class SignatureManager
    {
        public static string AddSignature(SAPbobsCOM.Company oCompany, DocumentSignatureSendModel signature)
        {
            bool saveOnlySignature = false;
            string fileName;
            string newDocPath;
            int attachedDoc;
            string _error = "";

            #region DEBUG *************************************************
            //SAPSignaturesAdd.DEBUGCreateSingatureListElements(oCompany);
            //SAPSignaturesAdd.AddSignature(oCompany, new DocumentSignatureSendModel() { docType = (int)DocumentTypeModel.DocumentTypeEnum.Albaran, docEntry = "2360" });
            //SAPSignaturesAdd.AddSignature(oCompany, new DocumentSignatureSendModel() { docType = (int)DocumentTypeModel.DocumentTypeEnum.Traslados, docEntry = "382" });
            #endregion ****************************************************


            if ((signature != null) && (signature.signature != null))
            {
                bool success = true;
                //Saving signature image to a temporary file
                fileName = SaveSignatureAsPng(new MemoryStream(signature.signature), signature.docType.ToString() + "-" + signature.docEntry);

                if (!string.IsNullOrWhiteSpace(fileName))
                {

                    //Este SWITCH se usa para determinar si queremos guardar sólo el fichero de imagen con la firma, o el documento firmado, y añadir previamente datos extras a los documentos
                    switch (signature.docType)
                    {
                        case (int)DocumentTypeModel.DocumentTypeEnum.Albaran:
                            success = SAP.SAPDeliveries.SAPDeliveriesData.AddSignerInfo(oCompany, signature);
                            break;
                        case (int)DocumentTypeModel.DocumentTypeEnum.Epis:
                            saveOnlySignature = true;
                            break;
                    }

                    if (success)
                    {
                        if (saveOnlySignature)
                        {
                            //Creating an attachment with the signature in SAP
                            attachedDoc = SAPAttachments.AddAttachment(oCompany, Path.GetFullPath(fileName), fileName);
                            //Setting the file to delete after document is attached
                            newDocPath = fileName;
                        }
                        else
                        {
                            //Adding signature to crystal and saving as PDF
                            newDocPath = SAP.CrystalReports.CrystalReportToPDF.DocumentToPDF(oCompany, signature, fileName);
                            //Deleting temporary signature image file
                            DeleteFile(fileName);
                            //attaching document into SAP
                            attachedDoc = SAPAttachments.AddAttachment(oCompany, System.Configuration.ConfigurationManager.AppSettings.Get("TempPDFImageFolder"), newDocPath);
                        }

                        if (attachedDoc > -1)
                        {
                            //Attaching PDF document to document to sign
                            if (AddAttachmentToDocument(oCompany, signature, attachedDoc))
                            {
                                DeleteFile(newDocPath);
                                //Updating DOI_FIRMAS user table
                                SAPSignaturesAdd.UpdateSignature(oCompany, signature);
                                //Log
                                ErrorHandler.WriteInLog("* Signature or PDF saved to " + newDocPath, ErrorHandler.ErrorVerboseLevel.None);
                            }
                            else
                            {
                                _error = "-e003|No se ha podido anexar el PDF firmado al documento. Consulte con el Administrador del sistema.";
                            }
                        }
                        else
                        {
                            DeleteFile(newDocPath);
                            ErrorHandler.WriteInLog("* Attachment cannot be created.", ErrorHandler.ErrorVerboseLevel.None);
                            _error = "-e002|No se ha podido anexar el documento en SAP. Consulte con el Administrador del sistema.";
                        }
                    }
                    else
                    {
                        ErrorHandler.WriteInLog("* Error setting previous information to the document.", ErrorHandler.ErrorVerboseLevel.None);
                        _error = "-e004|Se ha producido un error al guardar la nueva información del documento en SAP. Consulte con el Administrador del sistema.";
                    }
                }
            }
            else
            {
                _error = "-e001|No se ha incluido la firma.";
            }

            //Notifing error
            if (!string.IsNullOrWhiteSpace(_error))
            {
                ErrorHandler.WriteInLog("Signature error " + _error.Replace("|", ": "), ErrorHandler.ErrorVerboseLevel.None);
                _error = _error.Replace("|", ": ");
            }

            return _error;
        }


        #region Signature image file **************************************************************************************
        public static string SaveSignatureAsPng(Stream signature, string _fileName)
        {
            string fileName = "";
            try
            {
                //fileName = HttpContext.Current.Server.MapPath("~") + "Reports/sgntrtmpfl.jpg";
                fileName = System.Configuration.ConfigurationManager.AppSettings.Get("TempSignatureImageFolder") + "sgntr_" + _fileName + ".jpg";
                FileStream file = File.Create(fileName);
                signature.CopyTo(file);
                file.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }

            return fileName;
        }

        private static void DeleteFile(string fileName)
        {
            try
            {
                File.Delete(fileName);
            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }
        }
        #endregion ********************************************************************************************************

        #region attachments ***********************************************************************************************
        private static bool AddAttachmentToDocument(SAPbobsCOM.Company oCompany, DocumentSignatureSendModel signature, int idAtt)
        {
            bool success = false;
            switch (signature.docType)
            {
                case (int)DocumentTypeModel.DocumentTypeEnum.Albaran:
                    success = SAP.SAPDeliveries.SAPDeliveriesData.AddAtachment(oCompany, signature.docEntry, idAtt);
                    //success = success && SAP.SAPDeliveries.SAPDeliveriesData.AddSignerInfo(oCompany, signature);
                    break;
                case (int)DocumentTypeModel.DocumentTypeEnum.Traslados:
                    success = SAP.SAPTransfers.SAPInventoryTransfers.AddAtachment(oCompany, signature.docEntry, idAtt);
                    break;
                case (int)DocumentTypeModel.DocumentTypeEnum.Visitas:
                    success = SAP.SAPVisitors.SAPVisitorsData.AddAtachment(oCompany, signature.docEntry, idAtt);
                    break;
                case (int)DocumentTypeModel.DocumentTypeEnum.Epis:
                    success = SAP.SAPEpis.SAPEpisData.AddAtachment(oCompany, signature.docEntry, idAtt);
                    break;
            }

            return success;
        }
        #endregion ********************************************************************************************************

    }
}