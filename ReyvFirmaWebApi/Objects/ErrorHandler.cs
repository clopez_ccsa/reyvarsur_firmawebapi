﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Objects
{
    public static class ErrorHandler
    {
        private static string ProgramExecutionPath = HttpContext.Current.Server.MapPath("~");
        private static string LogPath = ProgramExecutionPath + "log";
        private static string LogNameSuffix = @"_webapi_log.txt";

        public enum ErrorVerboseLevel { None, OnlyError, ErrorAndData, Trace };
        private static ErrorVerboseLevel errorVerboseLevel = CheckErrorVerboseLevel();

        public enum MessageType { Error, Information };


        //You can use this list to add some messages before writing them at the same time. There are some methods
        //here to use it
        private static List<string> ehStackTrace = new List<string>();


        #region Initialization and setting methods ********************************************************************

        /// <summary>
        /// Initialization method. Set the initial verbose level.
        /// WebApi version: it reads the value from Web.config file
        /// </summary>
        /// <returns></returns>
        private static ErrorVerboseLevel CheckErrorVerboseLevel ()
        {
            ErrorVerboseLevel newErrorVerboseLevel = ErrorVerboseLevel.None;
            string tempString = System.Configuration.ConfigurationManager.AppSettings.Get("ErrorVerboseLevel");
            switch (tempString)
            {
                case "0":
                    newErrorVerboseLevel = ErrorVerboseLevel.None;
                    break;
                case "1":
                    newErrorVerboseLevel = ErrorVerboseLevel.OnlyError;
                    break;
                case "2":
                    newErrorVerboseLevel = ErrorVerboseLevel.ErrorAndData;
                    break;
                case "3":
                    newErrorVerboseLevel = ErrorVerboseLevel.Trace;
                    break;
                default:
                    newErrorVerboseLevel = ErrorVerboseLevel.OnlyError;
                    break;
            }

            return newErrorVerboseLevel;
        }

        /// <summary>
        /// Set the log file path if you don't want to use the default folder
        /// </summary>
        /// <param name="newLogPath"></param>
        public static void SetLogPath (string newLogPath)
        {
            LogPath = newLogPath;
        }

        #endregion ****************************************************************************************************


        #region write in external log *********************************************************************************
        /// <summary>
        /// Write a basic message in the log
        /// </summary>
        /// <param name="message"></param>
        /// <param name="messageVerboseLevel"></param>
        public static void WriteInLog (string message, ErrorVerboseLevel messageVerboseLevel)
        {
            System.IO.StreamWriter writer;

            //Check if the message verbose level is allowed by the main error handler verbose level
            if (messageVerboseLevel <= errorVerboseLevel)
            {
                //If path doesn't exist, create it
                if (!System.IO.Directory.Exists(LogPath))
                {
                    System.IO.Directory.CreateDirectory(LogPath);
                }

                using (writer = new System.IO.StreamWriter(LogPath + "/" + DateTime.Now.ToString("yyyy-MM-dd") + LogNameSuffix, true))
                {
                    //writer = new System.IO.StreamWriter(LogPath + "/" + DateTime.Now.ToString("yyyy-MM-dd") + LogNameSuffix, true);
                    writer.WriteLine(DateTime.Now.ToString() + ": " + message);
                    writer.Close();
                }
            }
        }


        /// <summary>
        /// Write a multiline basic text in the log
        /// </summary>
        /// <param name="message"></param>
        /// <param name="messageVerboseLevel"></param>
        public static void WriteInLog(List<string> message, ErrorVerboseLevel messageVerboseLevel)
        {
            System.IO.StreamWriter writer;
            int cont;

            //Check if the message verbose level is allowed by the main error handler verbose level
            if (messageVerboseLevel <= errorVerboseLevel)
            {
                //If path doesn't exist, create it
                if (!System.IO.Directory.Exists(LogPath))
                {
                    System.IO.Directory.CreateDirectory(LogPath);
                }

                writer = new System.IO.StreamWriter(LogPath + "/" + DateTime.Now.ToString("yyyy-MM-dd") + LogNameSuffix, true);

                writer.WriteLine(DateTime.Now.ToString() + "----------------------");
                for (cont = 0; cont < message.Count; cont++)
                {
                    writer.WriteLine(message[cont]);
                }
                writer.WriteLine("---------------------------------------------------------");
                writer.Close();
            }
        }

        public static void WriteInLog(Exception exception, string sError, ErrorVerboseLevel messageVerboseLevel)
        {
            System.IO.StreamWriter writer;

            if (messageVerboseLevel <= errorVerboseLevel)
            {
                //If path doesn't exist, create it
                if (!System.IO.Directory.Exists(LogPath))
                {
                    System.IO.Directory.CreateDirectory(LogPath);
                }

                writer = new System.IO.StreamWriter(LogPath + "/" + DateTime.Now.ToString("yyyy-MM-dd") + LogNameSuffix, true);
                writer.WriteLine(DateTime.Now.ToString() + "---Error: " + sError + " - " + exception.Message + 
                    Environment.NewLine + "TrazaError: " + exception.StackTrace);
                writer.Close();
            }
        }

        /*public static void WriteInDB(SAPbobsCOM.Company oCompany, Exception exception, string fileName, string sError,
                                     ErrorVerboseLevel messageVerboseLevel)
        {
            SAPbobsCOM.CompanyService oCompanyService;
            SAPbobsCOM.GeneralService oGeneralService;
            SAPbobsCOM.GeneralData oHeader;
            string sKey;

            try
            {
                oCompanyService = oCompany.GetCompanyService();
                oGeneralService = oCompanyService.GetGeneralService("DOI_LOG");
                oHeader = (SAPbobsCOM.GeneralData)(oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);

                sKey = _GetNextCode("@DOI_OLOG", "Code", null, 10);
                oHeader.SetProperty("DocEntry", sKey);

                oHeader.SetProperty("U_DOI_UsrCode", oCompany.UserSignature.ToString());
                oHeader.SetProperty("U_DOI_Date", DateTime.Now);
                oHeader.SetProperty("U_DOI_TypeMsg", MessageType.Error);
                oHeader.SetProperty("U_DOI_File", fileName);
                oHeader.SetProperty("U_DOI_Text", sError);
                oHeader.SetProperty("U_DOI_Message", exception.Message);
                oHeader.SetProperty("U_DOI_StackTrace", exception.StackTrace);
            }
            catch (Exception ex)
            {
                WriteInLog(ex, "Error guardando log en BBDD", ErrorVerboseLevel.OnlyError);
            }
        }*/
        #endregion *****************************************************************************************************


        #region handling local stack trace list ************************************************************************

        public static void AddTrace (string stString)
        {
            ehStackTrace.Add(stString);
        }

        public static void ClearTrace()
        {
            ehStackTrace.Clear();
        }

        public static void WriteTrace()
        {
            WriteInLog(ehStackTrace, ErrorVerboseLevel.Trace);
        }
        #endregion *****************************************************************************************************
    }
}