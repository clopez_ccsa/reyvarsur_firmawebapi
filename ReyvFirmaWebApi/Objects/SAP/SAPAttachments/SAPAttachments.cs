﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Objects.SAP.SAPAttachments
{
    public static class SAPAttachments
    {
        public static int AddAttachment(SAPbobsCOM.Company oCompany, string path, string fileName)
        {
            int attId = -1;
            string _error;
            int iError;
            SAPbobsCOM.Attachments2 oAttachment = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oAttachments2);

            oAttachment.Lines.Add();
            oAttachment.Lines.FileName = Path.GetFileNameWithoutExtension(fileName);
            oAttachment.Lines.FileExtension = Path.GetExtension(fileName).Substring(1);
            oAttachment.Lines.SourcePath = Path.GetDirectoryName(fileName);
            oAttachment.Lines.Override = SAPbobsCOM.BoYesNoEnum.tYES;

            oAttachment.Add();
            oCompany.GetLastError(out iError, out _error);
            if (iError == 0)
                attId = int.Parse(oCompany.GetNewObjectKey());
            else
            {
                ErrorHandler.WriteInLog("Attaching file to document. Error " + iError + ": " + _error, ErrorHandler.ErrorVerboseLevel.None);
            }

            return attId;
        }

        public static int AddAttachments(SAPbobsCOM.Company oCompany, List<string> filesList)
        {
            int attId = -1;
            bool isFirst = true;
            string _error;
            int iError;
            SAPbobsCOM.Attachments2 oAttachment = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oAttachments2);

            for (int i = 0; i < filesList.Count; i++)
            {
                if (isFirst)
                    isFirst = false;
                else
                    oAttachment.Lines.Add();

                oAttachment.Lines.FileName = Path.GetFileNameWithoutExtension(filesList[i]);
                oAttachment.Lines.FileExtension = Path.GetExtension(filesList[i]).Substring(1);
                oAttachment.Lines.SourcePath = Path.GetDirectoryName(filesList[i]);
                oAttachment.Lines.Override = SAPbobsCOM.BoYesNoEnum.tYES;
            }

            oAttachment.Add();
            oCompany.GetLastError(out iError, out _error);
            if (iError == 0)
                attId = int.Parse(oCompany.GetNewObjectKey());
            else
            {
                ErrorHandler.WriteInLog("Attaching file to document. Error " + iError + ": " + _error, ErrorHandler.ErrorVerboseLevel.None);
            }

            return attId;
        }
    }
}