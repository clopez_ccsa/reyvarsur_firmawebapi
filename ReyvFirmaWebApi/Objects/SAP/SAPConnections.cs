﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Objects.SAP
{
    public static class SAPConnections
    {

        #region Internal functions **************************************************************************************

        private static SAPbobsCOM.BoDataServerTypes GetServerType ()
        {
            string _dbServerTypeString = ConfigurationManager.AppSettings.Get("ServerType");
            SAPbobsCOM.BoDataServerTypes _dbServerType;

            
            switch (_dbServerTypeString)
            {
                case "MSSQL":
                    _dbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL;
                    break;
                case "MSSQL2005":
                    _dbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2005;
                    break;
                case "MSSQL2008":
                    _dbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
                    break;
                case "MSSQL2012":
                    _dbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2012;
                    break;
                case "MSSQL2014":
                    _dbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2014;
                    break;
                case "MSSQL2016":
                    _dbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2016;
                    break;
                case "HANADB":
                    _dbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                    break;
                default:
                    _dbServerType = 0;
                    ErrorHandler.WriteInLog("Server type not valid: " + _dbServerTypeString, ErrorHandler.ErrorVerboseLevel.None);
                    break;
            }

            return _dbServerType;
        }

        #endregion ******************************************************************************************************


        private static bool CheckMobilityUser (CompanyConnection companyConnection, string _user)
        {
            string sql;
            SAPbobsCOM.Recordset recordSet;
            bool isMobilityUser = false;

            sql = string.Format("SELECT \"MobileUser\" FROM \"OUSR\" WHERE \"USER_CODE\" = '{0}'", _user);
            recordSet = companyConnection.GetCompany(false).GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            recordSet.DoQuery(sql);

            if (!recordSet.EoF)
            {
                recordSet.MoveFirst();
                if (recordSet.Fields.Item("MobileUser").Value == "Y")
                    isMobilityUser = true;
            }

            return isMobilityUser;
        }

        /// <summary>
        /// Checks if an user is registered on SAP, using his/her SAP user and password
        /// (previously it checked it on the UDO of mobile users) and returns an open sesion 
        /// </summary>
        /// <param name="_sapUsername"></param>
        /// <param name="_sapPassword"></param>
        /// <returns>-1 connection error, 0 = Not registered, 1 = Registered, 2 = Registered but without license</returns>
        public static int CheckUserOnSAP(string _sapUsername, string _sapPassword,
                                           out CompanyConnection companyConnection)
        {
            int _isSAPUser = 0;
            
            string _licenseServer = ConfigurationManager.AppSettings.Get("LicenseServer");
            string _server = ConfigurationManager.AppSettings.Get("Server");
            string _companyDB = ConfigurationManager.AppSettings.Get("DB");
            //SAPbobsCOM.BoDataServerTypes _dbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
            SAPbobsCOM.BoDataServerTypes _dbServerType = GetServerType();

            int errCode;
            string errMsg;
            List<string> errMsgList;

            if (_dbServerType > 0)
            {
                ErrorHandler.WriteInLog("Logging " + _sapUsername + " on SAP", ErrorHandler.ErrorVerboseLevel.Trace);
                companyConnection = new CompanyConnection(_licenseServer, _server, _companyDB, _sapUsername, _sapPassword, _dbServerType);
                int _connectionError = companyConnection.Connect();



                if (_connectionError == 0)
                {

                    try
                    {
                        ErrorHandler.WriteInLog("User connected. Mobility user check.", ErrorHandler.ErrorVerboseLevel.ErrorAndData);
                        if (CheckMobilityUser(companyConnection, _sapUsername))
                            _isSAPUser = 1;
                        else
                            ErrorHandler.WriteInLog(string.Format("User {0} tried to log on, but is not mobility user.",
                                                                  _sapUsername), ErrorHandler.ErrorVerboseLevel.None);


                    }
                    catch (Exception ex)
                    {
                        companyConnection.Disconnect();
                        companyConnection = null;
                        ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                        _isSAPUser = -1;
                    }

                }
                else
                {
                    companyConnection.GetCompany(false).GetLastError(out errCode, out errMsg);
                    errMsgList = new List<string>();
                    errMsgList.Add("Server: " + _server);
                    errMsgList.Add("Company DB: " + _companyDB);
                    errMsgList.Add("User Name: " + _sapUsername);
                    errMsgList.Add("Password: " +
                                    _sapPassword.Substring(0, 1) + "**" + _sapPassword.Substring(_sapPassword.Length - 1));
                    errMsgList.Add("DB Server type: " + _dbServerType);
                    errMsgList.Add("   Error code: " + errCode);
                    errMsgList.Add("   Error msg: " + errMsg);
                    ErrorHandler.WriteInLog(errMsgList, ErrorHandler.ErrorVerboseLevel.OnlyError);
                    errMsgList.Clear();

                    switch (errCode)
                    {
                        case 100000048:
                            _isSAPUser = 2;
                            break;
                        case -4008:
                            _isSAPUser = 0;
                            break;
                        default:
                            _isSAPUser = -1;
                            break;
                    }
                    companyConnection.Disconnect();
                    companyConnection = null;
                }
            }
            else
            {
                companyConnection = null;
                _isSAPUser = -1;
            }


            return _isSAPUser;
        }


        


        /// <summary>
        /// Tries to connect an user to SAP. it will return the connection error code, and CompanyConnection if it
        /// is sucessful
        /// </summary>
        /// <param name="_user">SAP username</param>
        /// <param name="_password">SAP password</param>
        /// <param name="companyConnection">It returns the connection data for the connection list</param>
        /// <returns>Returns the error code</returns>
        public static int MakeConnection(string _user, string _password, out CompanyConnection cConnection)
        {
            string _licenseServer = ConfigurationManager.AppSettings.Get("LicenseServer");
            string _server = ConfigurationManager.AppSettings.Get("Server");
            string _companyDB = ConfigurationManager.AppSettings.Get("DB");
            //SAPbobsCOM.BoDataServerTypes _dbServerType = SAPbobsCOM.BoDataServerTypes.dst_MSSQL2008;
            SAPbobsCOM.BoDataServerTypes _dbServerType = GetServerType();

            int errCode = 0;
            string errMsg;
            List<string> errMsgList;


            if (_dbServerType > 0)
            {
                cConnection = new CompanyConnection(_licenseServer, _server, _companyDB, _user, _password, _dbServerType);

                int _error = cConnection.Connect();
                if (_error == 0)
                {


                }
                else
                {
                    cConnection = null;
                    cConnection.GetCompany(false).GetLastError(out errCode, out errMsg);
                    errMsgList = new List<string>();
                    errMsgList.Add("Conectando!!");
                    errMsgList.Add("Server: " + _server);
                    errMsgList.Add("Company DB: " + _companyDB);
                    errMsgList.Add("User Name: " + _user);
                    errMsgList.Add("Password: " +
                                    _password.Substring(0, 1) + "**" + _password.Substring(_password.Length - 1));
                    errMsgList.Add("DB Server type: " + _dbServerType);
                    errMsgList.Add("   Error code: " + errCode);
                    errMsgList.Add("   Error msg: " + errMsg);
                    ErrorHandler.WriteInLog(errMsgList, ErrorHandler.ErrorVerboseLevel.Trace);
                    errMsgList.Clear();
                }

                errCode = _error;
            }
            else
            {
                errCode = -1;
                cConnection = null;
            }

            return errCode;
        }
        

        
    }
}