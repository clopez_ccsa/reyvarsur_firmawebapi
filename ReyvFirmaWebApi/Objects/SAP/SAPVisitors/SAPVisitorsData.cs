﻿using ReyvFirmaWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Objects.SAP.SAPVisitors
{
    public static class SAPVisitorsData
    {
        #region INFO CONVERTERS *****************************************************************************************
        private static VisitorDataModel GetModelFromRecordset(SAPbobsCOM.Company oCompany, SAPbobsCOM.Recordset _recordSet)
        {
            VisitorDataModel visit = new VisitorDataModel();
            int debugInt;
            
            visit.docType = (int)DocumentTypeModel.DocumentTypeEnum.Visitas;

            visit.Id = _recordSet.Fields.Item("DocNum").Value.ToString();
            visit.docNum = _recordSet.Fields.Item("DocNum").Value.ToString();
            visit.cardCode = _recordSet.Fields.Item("CardCode").Value;
            visit.cardName = _recordSet.Fields.Item("CardName").Value;
            visit.docDate = _recordSet.Fields.Item("DocDate").Value;
            
            visit.visitorId = _recordSet.Fields.Item("VisitorCode").Value;
            visit.visitorName = _recordSet.Fields.Item("VisitorName").Value;
            visit.inChargeId = _recordSet.Fields.Item("inChargeName").Value;
            visit.inChargeName = _recordSet.Fields.Item("inChargeName").Value;
            visit.dni = _recordSet.Fields.Item("dni").Value;

            //¡¡¡¡ATENCIÓN!!!!: Por alguna razón, la lectura directa de la función IsNull del tipo Item falla. Debe leerse el valor del campo value antes de invocar a IsNull
            debugInt = _recordSet.Fields.Item("TimeIn").Value;
            if (_recordSet.Fields.Item("TimeIn").IsNull() == SAPbobsCOM.BoYesNoEnum.tNO)
                visit.timeIn = DateTime.ParseExact(_recordSet.Fields.Item("TimeIn").Value.ToString("0000"),"Hmm", System.Globalization.CultureInfo.InvariantCulture);
            debugInt = _recordSet.Fields.Item("TimeOut").Value;
            if (_recordSet.Fields.Item("TimeOut").IsNull() == SAPbobsCOM.BoYesNoEnum.tNO)
                visit.timeOut = DateTime.ParseExact(_recordSet.Fields.Item("TimeOut").Value.ToString("0000"), "Hmm", System.Globalization.CultureInfo.InvariantCulture);
            

            return visit;
        }
        #endregion *******************************************************************************************************

        #region Deliveries getters ***************************************************************************************
        /// <summary>
        /// Get a complete list of deliveries
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="userCode"></param>
        /// <returns></returns>
        public static List<DocumentDataModel> GetVisitorsList(SAPbobsCOM.Company oCompany, string userCode)
        {
            List<DocumentDataModel> visitorsList = new List<DocumentDataModel>();
            SAPbobsCOM.Recordset recordSet;
            VisitorDataModel tempVisitors;

            string userCodeFilter = "";
            string sql = "";

            if ((oCompany != null) && (oCompany.Connected))
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Adding sales person filter if valid
                    if (!string.IsNullOrWhiteSpace(userCode))
                        userCodeFilter = string.Format(SAPVisitorsQuerys.GetVisitorsList_UserCodeFilter, userCode);


                    sql = string.Format(SAPVisitorsQuerys.GetvisitorsList, (int)DocumentTypeModel.DocumentTypeEnum.Visitas, userCodeFilter);

                    if (!string.IsNullOrWhiteSpace(sql))
                    {
                        recordSet.DoQuery(sql);
                        ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                        if (recordSet.RecordCount > 0)
                        {
                            recordSet.MoveFirst();
                            while (!recordSet.EoF)
                            {
                                tempVisitors = GetModelFromRecordset(oCompany, recordSet);
                                if (tempVisitors != null)
                                    visitorsList.Add(tempVisitors);
                                recordSet.MoveNext();
                            }
                        }
                    }

                    //Adding a couple of data for debug purposses
                    /*visitorsList.Add(new VisitorDataModel()
                    {
                        Id = "00001",
                        docType = (int)DocumentTypeModel.DocumentTypeEnum.Visitas,
                        docNum = "V00001",
                        cardName = "Tomates SA",
                        docDate = DateTime.Now,
                        nItems = 7,
                        visitorName = "Alejandro Morán",
                        inChargeName = "Alex M",
                        dni = "48888888Z",
                        timeIn = DateTime.Now,
                        timeOut = DateTime.Now.AddHours(2)
                    });
                    visitorsList.Add(new VisitorDataModel()
                    {
                        Id = "00002",
                        docType = (int)DocumentTypeModel.DocumentTypeEnum.Visitas,
                        docNum = "V00002",
                        cardName = "Ketchup SA",
                        docDate = DateTime.Now.AddDays(1),
                        nItems = 22,
                        visitorName = "Pedro Delegado",
                        inChargeName = "Jesús",
                        dni = "28888888A",
                        timeIn = DateTime.Now.AddHours(2),
                        timeOut = DateTime.Now.AddHours(4)
                    });*/
                }
                catch (Exception ex)
                {
                    visitorsList = null;
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return visitorsList;
        }

        #endregion *******************************************************************************************************

        #region Attachments **********************************************************************************************
        public static bool AddAtachment(SAPbobsCOM.Company oCompany, string docEntry, int idAtt)
        {
            SAPbobsCOM.CompanyService oCompService;
            SAPbobsCOM.GeneralService oGeneralService;
            SAPbobsCOM.GeneralDataParams oGeneralParams;
            SAPbobsCOM.GeneralData oGeneralData;

            string _error;
            int iError;
            bool success = false;

            try
            {
                oCompService = oCompany.GetCompanyService();
                oGeneralService = oCompService.GetGeneralService("DOI_VISITAS");
                //Obteniendo la línea
                oGeneralParams = oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams);
                oGeneralParams.SetProperty("Code", docEntry);
                oGeneralData = oGeneralService.GetByParams(oGeneralParams);
                //Modificando los datos de la línea
                oGeneralData.SetProperty("U_DOI_FIRMA", idAtt);
                oGeneralService.Update(oGeneralData);

                oCompany.GetLastError(out iError, out _error);
                if (iError == 0)
                    success = true;
                else
                {
                    ErrorHandler.WriteInLog("Attaching PDF to Stock Transfer. Error " + iError + ": " + _error, ErrorHandler.ErrorVerboseLevel.None);
                }
            } catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }
            

            return success;
        }
        #endregion *******************************************************************************************************
    }
}