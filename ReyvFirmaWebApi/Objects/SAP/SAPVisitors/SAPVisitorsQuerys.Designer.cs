﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReyvFirmaWebApi.Objects.SAP.SAPVisitors {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class SAPVisitorsQuerys {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal SAPVisitorsQuerys() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ReyvFirmaWebApi.Objects.SAP.SAPVisitors.SAPVisitorsQuerys", typeof(SAPVisitorsQuerys).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a SELECT &quot;OVIS&quot;.&quot;U_DOI_FECHA&quot; AS &quot;DocDate&quot;, &quot;OVIS&quot;.&quot;Code&quot; AS &quot;DocNum&quot;,
        ///       &quot;OVIS&quot;.&quot;U_DOI_OCRD&quot; AS &quot;CardCode&quot;, &quot;OVIS&quot;.&quot;U_DOI_OCRDNa&quot; AS &quot;CardName&quot;,
        ///       &quot;OVIS&quot;.&quot;U_DOI_OCPR&quot; AS &quot;VisitorCode&quot;, &quot;OVIS&quot;.&quot;U_DOI_OCPRNa&quot; AS &quot;VisitorName&quot;,
        ///       &quot;OVIS&quot;.&quot;U_DOI_DNI&quot; AS &quot;dni&quot;,
        ///       &quot;OVIS&quot;.&quot;U_DOI_OHEM&quot; AS &quot;inChargeCode&quot;, &quot;OVIS&quot;.&quot;U_DOI_OHEMNa&quot; AS &quot;inChargeName&quot;,
        ///       &quot;OVIS&quot;.&quot;U_DOI_HORA_IN&quot; AS &quot;TimeIn&quot;, &quot;OVIS&quot;.&quot;U_DOI_HORA_OUT&quot; AS &quot;TimeOut&quot;
        ///FROM &quot;@DOI_FIRMAS&quot; AS &quot;S&quot;
        ///LEFT JOIN &quot;@DOI_VISITAS&quot; AS &quot;OVIS&quot; ON &quot;S&quot;.&quot; [resto de la cadena truncado]&quot;;.
        /// </summary>
        internal static string GetvisitorsList {
            get {
                return ResourceManager.GetString("GetvisitorsList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a AND &quot;OVIS&quot;.&quot;U_DOI_OUSR&quot; = &apos;{0}&apos;.
        /// </summary>
        internal static string GetVisitorsList_UserCodeFilter {
            get {
                return ResourceManager.GetString("GetVisitorsList_UserCodeFilter", resourceCulture);
            }
        }
    }
}
