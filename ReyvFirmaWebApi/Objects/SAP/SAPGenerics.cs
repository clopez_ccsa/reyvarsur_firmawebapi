﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Objects.SAP
{
    public static class SAPGenerics
    {
        public static string UDOGetNextNumericStringCode(SAPbobsCOM.Company oCompany, string table, string codeColumn, string outFormat)
        {
            SAPbobsCOM.Recordset recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            string sql;
            string codeValue;
            int iCodeValue;

            if ((oCompany != null) && oCompany.Connected)
            {
                try
                {
                    sql = string.Format(SAPGenericsQuerys.GetLastTableCode, table, codeColumn);
                    recordSet.DoQuery(sql);

                    if (!recordSet.EoF)
                    {
                        recordSet.MoveFirst();
                        codeValue = recordSet.Fields.Item(codeColumn).Value;
                        if (int.TryParse(codeValue, out iCodeValue))
                        {
                            iCodeValue++;
                            codeValue = iCodeValue.ToString(outFormat);
                            return codeValue;
                        }
                        else
                        {
                            throw new Exception(string.Format("Table code is not a numeric value ({0})", codeValue));
                        }
                    }
                    else
                    {
                        //There is no elements in the table. Making the first
                        iCodeValue = 1;
                        codeValue = iCodeValue.ToString(outFormat);
                        return codeValue;
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return "";
        }
    }
}