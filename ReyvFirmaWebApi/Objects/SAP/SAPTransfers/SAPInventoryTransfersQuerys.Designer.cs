﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReyvFirmaWebApi.Objects.SAP.SAPTransfers {
    using System;
    
    
    /// <summary>
    ///   Clase de recurso fuertemente tipado, para buscar cadenas traducidas, etc.
    /// </summary>
    // StronglyTypedResourceBuilder generó automáticamente esta clase
    // a través de una herramienta como ResGen o Visual Studio.
    // Para agregar o quitar un miembro, edite el archivo .ResX y, a continuación, vuelva a ejecutar ResGen
    // con la opción /str o recompile su proyecto de VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class SAPInventoryTransfersQuerys {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal SAPInventoryTransfersQuerys() {
        }
        
        /// <summary>
        ///   Devuelve la instancia de ResourceManager almacenada en caché utilizada por esta clase.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ReyvFirmaWebApi.Objects.SAP.SAPTransfers.SAPInventoryTransfersQuerys", typeof(SAPInventoryTransfersQuerys).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Reemplaza la propiedad CurrentUICulture del subproceso actual para todas las
        ///   búsquedas de recursos mediante esta clase de recurso fuertemente tipado.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a SELECT &quot;OWTR&quot;.&quot;DocEntry&quot;, &quot;OWTR&quot;.&quot;DocNum&quot;, &quot;OWTR&quot;.&quot;CardCode&quot;, &quot;OWTR&quot;.&quot;CardName&quot;,
        ///       &quot;OWTR&quot;.&quot;DocDueDate&quot;, count(&quot;WTR1&quot;.&quot;LineNum&quot;) AS &quot;NArticulos&quot;,
        ///       &quot;OWTR&quot;.&quot;Filler&quot; AS &quot;AlmacenOrigId&quot;, &quot;WH1&quot;.&quot;WhsName&quot; AS &quot;AlmacenOrig&quot;,
        ///       &quot;OWTR&quot;.&quot;ToWhsCode&quot; AS &quot;AlmacenDestId&quot;, &quot;WH2&quot;.&quot;WhsName&quot; AS &quot;AlmacenDest&quot;
        ///FROM &quot;@DOI_FIRMAS&quot; AS &quot;S&quot;
        ///LEFT JOIN &quot;OWTR&quot; ON &quot;S&quot;.&quot;U_DocEntry&quot; = &quot;OWTR&quot;.&quot;DocEntry&quot;
        ///LEFT JOIN &quot;WTR1&quot; ON &quot;OWTR&quot;.&quot;DocEntry&quot; = &quot;WTR1&quot;.&quot;DocEntry&quot;
        ///LEFT JOIN &quot;OWHS&quot; AS &quot;WH1&quot; ON &quot;OWTR&quot;.&quot;Filler&quot; = &quot;WH1&quot;.&quot;WhsC [resto de la cadena truncado]&quot;;.
        /// </summary>
        internal static string GetInventoryTransferList {
            get {
                return ResourceManager.GetString("GetInventoryTransferList", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Busca una cadena traducida similar a  AND &quot;OWTR&quot;.&quot;U_DOI_UsrFCode&quot; = &apos;{0}&apos; .
        /// </summary>
        internal static string GetInventoryTransferList_UserCodeFilter {
            get {
                return ResourceManager.GetString("GetInventoryTransferList_UserCodeFilter", resourceCulture);
            }
        }
    }
}
