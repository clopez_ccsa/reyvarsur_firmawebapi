﻿using ReyvFirmaWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Objects.SAP.SAPTransfers
{
    public static class SAPInventoryTransfers
    {
        #region INFO CONVERTERS *****************************************************************************************
        private static InventoryTransferDataModel GetModelFromRecordset(SAPbobsCOM.Company oCompany, SAPbobsCOM.Recordset _recordSet)
        {
            InventoryTransferDataModel delivery = new InventoryTransferDataModel();

            delivery.docType = (int)DocumentTypeModel.DocumentTypeEnum.Traslados;

            delivery.Id = _recordSet.Fields.Item("DocEntry").Value.ToString();
            delivery.docNum = _recordSet.Fields.Item("DocNum").Value.ToString();
            delivery.cardCode = _recordSet.Fields.Item("CardCode").Value;
            delivery.cardName = _recordSet.Fields.Item("CardName").Value;
            delivery.docDate = _recordSet.Fields.Item("DocDueDate").Value;
            delivery.nItems = _recordSet.Fields.Item("NArticulos").Value;

            delivery.almacenOrigID = _recordSet.Fields.Item("AlmacenOrigId").Value;
            delivery.almacenOrig = _recordSet.Fields.Item("AlmacenOrig").Value;
            delivery.almacenDestID = _recordSet.Fields.Item("AlmacenDestId").Value;
            delivery.almacenDest = _recordSet.Fields.Item("AlmacenDest").Value;

            return delivery;
        }
        #endregion *******************************************************************************************************

        #region Deliveries getters ***************************************************************************************
        /// <summary>
        /// Get a complete list of deliveries
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="userCode"></param>
        /// <returns></returns>
        public static List<DocumentDataModel> GetInventoryTransferList(SAPbobsCOM.Company oCompany, string userCode)
        {
            List<DocumentDataModel> transfersList = new List<DocumentDataModel>();
            SAPbobsCOM.Recordset recordSet;
            DocumentDataModel tempTransfer;

            string userCodeFilter = "";
            string sql = "";

            if ((oCompany != null) && (oCompany.Connected))
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Adding sales person filter if valid
                    if (!string.IsNullOrWhiteSpace(userCode))
                        userCodeFilter = string.Format(SAPInventoryTransfersQuerys.GetInventoryTransferList_UserCodeFilter, userCode);


                    sql = string.Format(SAPInventoryTransfersQuerys.GetInventoryTransferList,
                                        (int)DocumentTypeModel.DocumentTypeEnum.Traslados, userCodeFilter);

                    if (!string.IsNullOrWhiteSpace(sql))
                    {
                        recordSet.DoQuery(sql);
                        ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                        if (recordSet.RecordCount > 0)
                        {
                            recordSet.MoveFirst();
                            while (!recordSet.EoF)
                            {
                                tempTransfer = GetModelFromRecordset(oCompany, recordSet);
                                if (tempTransfer != null)
                                    transfersList.Add(tempTransfer);
                                recordSet.MoveNext();
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    transfersList = null;
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return transfersList;
        }

        #endregion *******************************************************************************************************

        #region Attachments **********************************************************************************************
        public static bool AddAtachment(SAPbobsCOM.Company oCompany, string docEntry, int idAtt)
        {
            int iDocEntry;
            string _error;
            int iError;
            bool success = false;

            if (int.TryParse(docEntry, out iDocEntry))
            {
                SAPbobsCOM.StockTransfer oTransfer = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oStockTransfer);
                oTransfer.GetByKey(iDocEntry);

                oTransfer.AttachmentEntry = idAtt;
                oTransfer.Update();

                oCompany.GetLastError(out iError, out _error);
                if (iError == 0)
                    success = true;
                else
                {
                    ErrorHandler.WriteInLog("Attaching PDF to Stock Transfer. Error " + iError + ": " + _error, ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            else
            {
                ErrorHandler.WriteInLog("Attaching PDF to Stock Transfer. Error: " + docEntry + " is not a valis DocEntry", ErrorHandler.ErrorVerboseLevel.None);
            }

            return success;
        }
        #endregion *******************************************************************************************************
    }
}