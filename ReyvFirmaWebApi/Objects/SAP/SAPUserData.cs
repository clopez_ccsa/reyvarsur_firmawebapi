﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Objects
{
    public class SAPUserData
    {
        private static void NameToFirstLastName(string completeName, out string _firstName, out string _lastName)
        {
            string[] elementosNombre = completeName.Split(' ');
            switch (elementosNombre.Length)
            {
                case 1:
                    _firstName = elementosNombre[0];
                    _lastName = "";
                    break;
                case 2:
                    _firstName = elementosNombre[0];
                    _lastName = elementosNombre[1];
                    break;
                case 3: //This will depend on the country :S
                    _firstName = elementosNombre[0];
                    _lastName = elementosNombre[1] + " " + elementosNombre[2];
                    break;
                default:
                    _firstName = elementosNombre[0];
                    for (int i = 1; i < elementosNombre.Length - 2; i++)
                    {
                        _firstName = _firstName + " " + elementosNombre[i];
                    }
                    _lastName = elementosNombre[elementosNombre.Length - 2] + " " +
                                elementosNombre[elementosNombre.Length - 1];
                    break;
            }
        }

        public static int GetFirstLastUserName(string token, string _user, out string _firstName, out string _lastName)
        {
            SAPbobsCOM.Company oCompany;
            CompanyConnection oCompanyConnection;
            SAPbobsCOM.Recordset recordSet;
            string sql;
            string userName;
            int _error = 0;

            _firstName = "";
            _lastName = "";

            //Obtenemos el oCompany de la conexión del usuario por su token
            oCompany = ConnectionsManager.GetCompanyByToken(token, false);
            if (oCompany != null)
            {
                //Comprobamos que el oCompany esté conectado (y si no, se conecta)
                if (!oCompany.Connected)
                {
                    oCompanyConnection = ConnectionsManager.GetCompanyConnectionByToken(token);
                    if (oCompanyConnection.Connect() != 0)
                    {
                        _error = -1;
                    }
                }

                //Con la conexión hecha, solicitamos la información
                recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                sql = string.Format("SELECT \"U_NAME\" FROM \"OUSR\" WHERE \"USER_CODE\" = '{0}'", _user);
                recordSet.DoQuery(sql);

                if (!recordSet.EoF)
                {
                    recordSet.MoveFirst();
                    userName = recordSet.Fields.Item("U_NAME").Value;

                    NameToFirstLastName(userName, out _firstName, out _lastName);
                } else
                {
                    _error = -2;
                }
            }

            return _error;
        }
    }
}