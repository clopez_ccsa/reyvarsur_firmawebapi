﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Objects.SAP
{
    public class SAPSalesPerson
    {
        public static void GetSalesId (string token, string user, out int idEmpleadoVentas)
        {
            SAPbobsCOM.Company oCompany;
            CompanyConnection oCompanyConnection;
            SAPbobsCOM.Recordset _recordset;
            string query;
            
            oCompany = ConnectionsManager.GetCompanyByToken(token, false);
            if (oCompany != null)
            {
                if (!oCompany.Connected)
                {
                    oCompanyConnection = ConnectionsManager.GetCompanyConnectionByToken(token);
                    if (oCompanyConnection.Connect() != 0)
                    {
                        throw new Exception("Unable to connect with SAP. Please, try again.");
                    }
                }
                _recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                query = string.Format(SAPSalesPersonQuerys.GetSalesPerson_FromUserId, user);

                _recordset.DoQuery(query);
                if (_recordset.RecordCount > 0)
                {
                    _recordset.MoveFirst();

                    if (_recordset.Fields.Item("salesPrson").Value == null)
                        idEmpleadoVentas = -1;
                    else
                        idEmpleadoVentas = _recordset.Fields.Item("salesPrson").Value;

                }
                else
                {
                    idEmpleadoVentas = -1;
                }
            }
            else
            {
                //soyJefeVentas = false;
                //idEmpleadoVentas = -1;
                throw new Exception("Unable to connect with SAP. Please, try again.");
            }
        }



        public static string GetSalesPersonName(SAPbobsCOM.Company oCompany, int idEmpleadoVentas)
        {
            SAPbobsCOM.Recordset _recordset;
            string query;
            string slpName = "";

            if ((oCompany != null) && oCompany.Connected)
            {
                _recordset = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                query = string.Format(SAPSalesPersonQuerys.GetSalesPersonName, idEmpleadoVentas);

                _recordset.DoQuery(query);
                if (_recordset.RecordCount > 0)
                {
                    _recordset.MoveFirst();
                    slpName = _recordset.Fields.Item("SlpName").Value;

                }
            }

            return slpName;
        }
    }
}