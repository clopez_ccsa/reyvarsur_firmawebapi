﻿using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using ReyvFirmaWebApi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Objects.SAP.CrystalReports
{
    public static class CrystalReportToPDF
    {
        public static readonly string rutaInstalacion = HttpContext.Current.Server.MapPath("~");

        #region GET template path ******************************************
        private static string GetDocTypePath(int docType)
        {
            string tempPath = "";
            switch (docType)
            {
                case (int)DocumentTypeModel.DocumentTypeEnum.Albaran:
                    tempPath = ConfigurationManager.AppSettings.Get("DeliveryReportTemplate");
                    break;
                case (int)DocumentTypeModel.DocumentTypeEnum.Traslados:
                    tempPath = ConfigurationManager.AppSettings.Get("TransferReportTemplate");
                    break;
                case (int)DocumentTypeModel.DocumentTypeEnum.Visitas:
                    tempPath = ConfigurationManager.AppSettings.Get("VisitorReportTemplate");
                    break;
                case (int)DocumentTypeModel.DocumentTypeEnum.Epis:
                    tempPath = ConfigurationManager.AppSettings.Get("EPISDeliveryReportTemplate");
                    break;
            }

            return tempPath;
        }

        private static string GetReportFileName(DocumentSignatureSendModel docInfo)
        {
            string tempPath = "";
            switch (docInfo.docType)
            {
                case (int)DocumentTypeModel.DocumentTypeEnum.Albaran:
                    tempPath = "Albaran-";
                    break;
                case (int)DocumentTypeModel.DocumentTypeEnum.Traslados:
                    tempPath = "Traslado-";
                    break;
                case (int)DocumentTypeModel.DocumentTypeEnum.Visitas:
                    tempPath = "Visita-";
                    break;
                case (int)DocumentTypeModel.DocumentTypeEnum.Epis:
                    tempPath = "EntregaEPIS-";
                    break;
                default:
                    tempPath = "Documento-";
                    break;
            }

            tempPath += docInfo.docEntry + "_" + DateTime.Now.ToString("ddMMyyyy_HHmm");

            return tempPath;
        }
        #endregion *********************************************************

        public static string DocumentToPDF(SAPbobsCOM.Company oCompany, DocumentSignatureSendModel documentType, string signPath)
        {
            string reportFileName = "";
            ReportDocument crystalReport = new ReportDocument();
            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            string _DBUserID;
            string _DBpwd;

            string templatePath = GetDocTypePath(documentType.docType);

            try
            {
                crystalReport.Load(rutaInstalacion + templatePath);

                _DBUserID = ConfigurationManager.AppSettings.Get("DBUserID");
                _DBpwd = SAPDesglose.Decrypt(ConfigurationManager.AppSettings.Get("DBpwd"));
                crystalReport.SetDatabaseLogon(_DBUserID, _DBpwd, oCompany.Server, oCompany.CompanyDB);
                //crystalReport.SetDatabaseLogon("sa", "SAPB1Admin");

                crConnectionInfo.DatabaseName = oCompany.CompanyDB;
                crConnectionInfo.ServerName = oCompany.Server;
                crConnectionInfo.UserID = _DBUserID;
                crConnectionInfo.Password = _DBpwd;

                foreach (Table crTabla in crystalReport.Database.Tables)
                {
                    crTabla.LogOnInfo.ConnectionInfo = crConnectionInfo;
                    crTabla.ApplyLogOnInfo(crTabla.LogOnInfo);
                }

                //crystalReport.Refresh();
                crystalReport.VerifyDatabase();

                //Setting document ID
                crystalReport.SetParameterValue("@DocKey", documentType.docEntry);
                //Adding signature image to the document
                crystalReport.DataDefinition.FormulaFields["ImagePath"].Text =
                    "\"" + signPath + "\"";
                    //"\"" + System.Configuration.ConfigurationManager.AppSettings.Get("TempSignatureImageFolder") + "sgntrtmpfl.jpg\"";

                //Saving report
                reportFileName = System.Configuration.ConfigurationManager.AppSettings.Get("TempPDFImageFolder") + GetReportFileName(documentType) + ".pdf";
                crystalReport.ExportToDisk(ExportFormatType.PortableDocFormat, reportFileName);
                //crystalReport.ExportToDisk(ExportFormatType.Excel, carpetaInformes + _reportFileName + ".xls");

                crystalReport.Close();
            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }

            return reportFileName;
        }
    }
}