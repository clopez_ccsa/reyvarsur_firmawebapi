﻿using ReyvFirmaWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Objects.SAP.SAPDeliveries
{
    public static class SAPDeliveriesData
    {

        #region INFO CONVERTERS *****************************************************************************************
        private static DeliveryDataModel GetModelFromRecordset(SAPbobsCOM.Company oCompany, SAPbobsCOM.Recordset _recordSet)
        {
            DeliveryDataModel delivery = new DeliveryDataModel();

            delivery.docType = (int)DocumentTypeModel.DocumentTypeEnum.Albaran;

            delivery.Id = _recordSet.Fields.Item("DocEntry").Value.ToString();
            delivery.docNum = _recordSet.Fields.Item("DocNum").Value.ToString();
            delivery.cardCode = _recordSet.Fields.Item("CardCode").Value;
            delivery.cardName = _recordSet.Fields.Item("CardName").Value;
            delivery.docDate = _recordSet.Fields.Item("DocDueDate").Value;
            delivery.nItems = _recordSet.Fields.Item("NArticulos").Value;

            delivery.docTotal = _recordSet.Fields.Item("DocTotal").Value;

            return delivery;
        }
        #endregion *******************************************************************************************************

        #region Deliveries getters ***************************************************************************************
        /// <summary>
        /// Get a complete list of deliveries
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="userCode"></param>
        /// <returns></returns>
        public static List<DocumentDataModel> GetDeliveriesList(SAPbobsCOM.Company oCompany, string userCode)
        {
            List<DocumentDataModel> deliveriesList = new List<DocumentDataModel>();
            SAPbobsCOM.Recordset recordSet;
            DocumentDataModel tempDelivery;

            string userCodeFilter = "";
            string sql = "";

            if ((oCompany != null) && (oCompany.Connected))
            {
                try
                {
                    #region DEBUG ***********************************************************************************************************************************************
                    //SAPSignatures.SAPSignaturesAdd.AddSignature(oCompany, new DocumentSignatureSendModel() { docType = (int)DocumentTypeModel.DocumentTypeEnum.Albaran, docEntry = "2360" });
                    //SAPSignatures.SAPSignaturesAdd.AddSignature(oCompany, new DocumentSignatureSendModel() { docType = (int)DocumentTypeModel.DocumentTypeEnum.Traslados, docEntry = "382" });
                    //SAPSignatures.SAPSignaturesAdd.AddSignature(oCompany, new DocumentSignatureSendModel() { docType = (int)DocumentTypeModel.DocumentTypeEnum.Albaran, docEntry = "2376" });
                    //SAPSignatures.SAPSignaturesAdd.AddSignature(oCompany, new DocumentSignatureSendModel() { docType = (int)DocumentTypeModel.DocumentTypeEnum.Albaran, docEntry = "2377" });
                    //SAPSignatures.SAPSignaturesAdd.AddSignature(oCompany, new DocumentSignatureSendModel() { docType = (int)DocumentTypeModel.DocumentTypeEnum.Albaran, docEntry = "2378" });
                    //SAPSignatures.SAPSignaturesAdd.AddSignature(oCompany, new DocumentSignatureSendModel() { docType = (int)DocumentTypeModel.DocumentTypeEnum.Traslados, docEntry = "385" });
                    //SAPSignatures.SAPSignaturesAdd.AddSignature(oCompany, new DocumentSignatureSendModel() { docType = (int)DocumentTypeModel.DocumentTypeEnum.Traslados, docEntry = "386" });
                    //SAPSignatures.SAPSignaturesAdd.AddSignature(oCompany, new DocumentSignatureSendModel() { docType = (int)DocumentTypeModel.DocumentTypeEnum.Traslados, docEntry = "387" });
                    #endregion **************************************************************************************************************************************************

                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Adding sales person filter if valid
                    if (!string.IsNullOrWhiteSpace(userCode))
                        userCodeFilter = string.Format(SAPDeliveriesQuerys.GetDeliveriesList_UserCodeFilter, userCode);


                    sql = string.Format(SAPDeliveriesQuerys.GetDeliveriesList, (int)DocumentTypeModel.DocumentTypeEnum.Albaran, userCodeFilter);

                    if (!string.IsNullOrWhiteSpace(sql))
                    {
                        recordSet.DoQuery(sql);
                        ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                        if (recordSet.RecordCount > 0)
                        {
                            recordSet.MoveFirst();
                            while (!recordSet.EoF)
                            {
                                tempDelivery = GetModelFromRecordset(oCompany, recordSet);
                                if (tempDelivery != null)
                                    deliveriesList.Add(tempDelivery);
                                recordSet.MoveNext();
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    deliveriesList = null;
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return deliveriesList;
        }

        #endregion *******************************************************************************************************

        #region Deliveries info update ***********************************************************************************
        public static bool AddSignerInfo(SAPbobsCOM.Company oCompany, DocumentSignatureSendModel signature)
        {
            int iDocEntry;
            string _error;
            int iError;
            bool success = false;

            if (signature != null)
            {
                try
                {
                    if (int.TryParse(signature.docEntry, out iDocEntry))
                    {
                        SAPbobsCOM.Documents oDelivery = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDeliveryNotes);
                        oDelivery.GetByKey(iDocEntry);

                        oDelivery.UserFields.Fields.Item("U_DOI_NOMBRE").Value = signature.firmaNombre;
                        oDelivery.UserFields.Fields.Item("U_DOI_APELLIDOS").Value = signature.firmaApellidos;
                        if (!string.IsNullOrWhiteSpace(signature.firmaNIF))
                            oDelivery.UserFields.Fields.Item("U_DOI_NIF").Value = signature.firmaNIF;
                        if (!string.IsNullOrWhiteSpace(signature.firmaMatricula))
                            oDelivery.UserFields.Fields.Item("U_DOI_MATRICULA").Value = signature.firmaMatricula;
                        oDelivery.Update();

                        oCompany.GetLastError(out iError, out _error);
                        if (iError == 0)
                            success = true;
                        else
                        {
                            ErrorHandler.WriteInLog("Updating signer data to delivery. Error " + iError + ": " + _error, ErrorHandler.ErrorVerboseLevel.None);
                        }
                    }
                    else
                    {
                        ErrorHandler.WriteInLog("Updating signer data to delivery. Error: " + signature.docEntry + " is not a valis DocEntry", ErrorHandler.ErrorVerboseLevel.None);
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            else
            {
                ErrorHandler.WriteInLog("Updating signer data to delivery error: no data to update.", ErrorHandler.ErrorVerboseLevel.None);
            }

            return success;
        }
        #endregion *******************************************************************************************************


        #region Attachments **********************************************************************************************
        public static bool AddAtachment(SAPbobsCOM.Company oCompany, string docEntry, int idAtt)
        {
            int iDocEntry;
            string _error;
            int iError;
            bool success = false;

            if (int.TryParse(docEntry, out iDocEntry))
            {
                SAPbobsCOM.Documents oDelivery = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDeliveryNotes);
                oDelivery.GetByKey(iDocEntry);

                oDelivery.AttachmentEntry = idAtt;
                oDelivery.Update();

                oCompany.GetLastError(out iError, out _error);
                if (iError == 0)
                    success = true;
                else
                {
                    ErrorHandler.WriteInLog("Attaching PDF to delivery. Error " + iError + ": " + _error, ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            else
            {
                ErrorHandler.WriteInLog("Attaching PDF to delivery. Error: " + docEntry + " is not a valis DocEntry", ErrorHandler.ErrorVerboseLevel.None);
            }


            return success;
        }
        #endregion *******************************************************************************************************
    }
}