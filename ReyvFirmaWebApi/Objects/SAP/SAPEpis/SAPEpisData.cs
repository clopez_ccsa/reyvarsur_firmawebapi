﻿using ReyvFirmaWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Objects.SAP.SAPEpis
{
    public static class SAPEpisData
    {
        #region INFO CONVERTERS *****************************************************************************************
        private static EpisDataModel GetModelFromRecordset(SAPbobsCOM.Company oCompany, SAPbobsCOM.Recordset _recordSet)
        {
            EpisDataModel epi = new EpisDataModel();

            epi.docType = (int)DocumentTypeModel.DocumentTypeEnum.Epis;

            epi.Id = _recordSet.Fields.Item("DocEntry").Value.ToString();
            epi.docNum = _recordSet.Fields.Item("DocNum").Value.ToString();
            epi.cardCode = _recordSet.Fields.Item("U_DOI_OHEM").Value;
            epi.cardName = _recordSet.Fields.Item("U_DOI_OHEMNa").Value;
            epi.docDate = _recordSet.Fields.Item("DocDate").Value;
            epi.nItems = _recordSet.Fields.Item("NArticulos").Value;

            return epi;
        }

        private static DocumentLineDataModel GetModelLineFromRecordset(SAPbobsCOM.Company oCompany, SAPbobsCOM.Recordset _recordSet)
        {
            DocumentLineDataModel epiLine = new DocumentLineDataModel();

            epiLine.Id = _recordSet.Fields.Item("ItemCode").Value.ToString();
            epiLine.Name = _recordSet.Fields.Item("Dscription").Value.ToString();
            epiLine.quantity = _recordSet.Fields.Item("Quantity").Value;

            return epiLine;
        }
        #endregion *******************************************************************************************************

        #region EPIS getters ***************************************************************************************
        /// <summary>
        /// Get a complete list of EPIS
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="userCode"></param>
        /// <returns></returns>
        public static List<DocumentDataModel> GetEpisList(SAPbobsCOM.Company oCompany, string userCode)
        {
            List<DocumentDataModel> episList = new List<DocumentDataModel>();
            SAPbobsCOM.Recordset recordSet;
            DocumentDataModel tempEpis;

            string userCodeFilter = "";
            string sql = "";

            if ((oCompany != null) && (oCompany.Connected))
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    //Adding sales person filter if valid
                    if (!string.IsNullOrWhiteSpace(userCode))
                        userCodeFilter = string.Format(SAPEpisQuerys.GetEpisExitList_UserCodeFilter, userCode);


                    sql = string.Format(SAPEpisQuerys.GetEpisExitList, (int)DocumentTypeModel.DocumentTypeEnum.Epis, userCodeFilter);

                    if (!string.IsNullOrWhiteSpace(sql))
                    {
                        recordSet.DoQuery(sql);
                        ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                        if (recordSet.RecordCount > 0)
                        {
                            recordSet.MoveFirst();
                            while (!recordSet.EoF)
                            {
                                tempEpis = GetModelFromRecordset(oCompany, recordSet);
                                if (tempEpis != null)
                                    episList.Add(tempEpis);
                                recordSet.MoveNext();
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    episList = null;
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return episList;
        }



        /// <summary>
        /// Get a complete list of EPIS
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="userCode"></param>
        /// <returns></returns>
        public static List<DocumentLineDataModel> GetEpisLinesList(SAPbobsCOM.Company oCompany, string docEntry)
        {
            List<DocumentLineDataModel> episList = new List<DocumentLineDataModel>();
            SAPbobsCOM.Recordset recordSet;
            DocumentLineDataModel tempEpisLine;

            string sql = "";

            if ((oCompany != null) && (oCompany.Connected) && !string.IsNullOrWhiteSpace(docEntry))
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    
                    sql = string.Format(SAPEpisQuerys.GetEpisLinesList, docEntry);

                    if (!string.IsNullOrWhiteSpace(sql))
                    {
                        recordSet.DoQuery(sql);
                        ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                        if (recordSet.RecordCount > 0)
                        {
                            recordSet.MoveFirst();
                            while (!recordSet.EoF)
                            {
                                tempEpisLine = GetModelLineFromRecordset(oCompany, recordSet);
                                if (tempEpisLine != null)
                                    episList.Add(tempEpisLine);
                                recordSet.MoveNext();
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    episList = null;
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return episList;
        }

        #endregion *******************************************************************************************************


        #region Attachments **********************************************************************************************
        public static bool AddAtachment(SAPbobsCOM.Company oCompany, string docEntry, int idAtt)
        {
            int iDocEntry;
            string _error;
            int iError;
            bool success = false;

            if (int.TryParse(docEntry, out iDocEntry))
            {
                SAPbobsCOM.Documents oGoodsIssue = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oInventoryGenExit);
                oGoodsIssue.GetByKey(iDocEntry);

                oGoodsIssue.AttachmentEntry = idAtt;
                oGoodsIssue.Update();

                oCompany.GetLastError(out iError, out _error);
                if (iError == 0)
                    success = true;
                else
                {
                    ErrorHandler.WriteInLog("Attaching signature image file to Inventory exit (EPIS). Error " + iError + ": " + _error, ErrorHandler.ErrorVerboseLevel.None);
                }
            }
            else
            {
                ErrorHandler.WriteInLog("Attaching signature image file to Inventory exit (EPIS). Error: " + docEntry + " is not a valis DocEntry", ErrorHandler.ErrorVerboseLevel.None);
            }

            return success;
        }
        #endregion *******************************************************************************************************

    }
}