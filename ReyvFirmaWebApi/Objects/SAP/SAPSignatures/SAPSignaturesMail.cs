﻿using ReyvFirmaWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Objects.SAP.SAPSignatures
{
    public static class SAPSignaturesMail
    {
        private class UserMailClass
        {
            public SAPbobsCOM.BoMsgRcpTypes userType;
            public string userName;
            public string userMail;
        }

        #region Get mails **********************************************************************************************************
        private static List<UserMailClass> GetBPContactMail(SAPbobsCOM.Company oCompany, int docType, string docEntry)
        {
            List<UserMailClass> contactDataList = new List<UserMailClass>();
            string cardCode;
            string contactMail;
            SAPbobsCOM.Recordset recordSet;
            string sql;

            try
            {
                //Initializing objects
                recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                switch(docType)
                {
                    case (int)DocumentTypeModel.DocumentTypeEnum.Albaran:
                        sql = string.Format(SAPSignaturesMailQuerys.GetBPMail_Documents, "ODLN", docEntry);
                        break;
                    case (int)DocumentTypeModel.DocumentTypeEnum.Traslados:
                        sql = string.Format(SAPSignaturesMailQuerys.GetBPMail_Documents, "OWTR", docEntry);
                        break;
                    default:
                        sql = "";
                        break;
                }
                

                if (!string.IsNullOrWhiteSpace(sql))
                {
                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        recordSet.MoveFirst();
                        while (!recordSet.EoF)
                        {
                            cardCode = recordSet.Fields.Item("CardCode").Value.ToString();
                            contactMail = recordSet.Fields.Item("E_MailL").Value;

                            if (!string.IsNullOrWhiteSpace(contactMail))
                            {
                                if (!contactDataList.Any(x => x.userMail == contactMail))
                                    contactDataList.Add(new UserMailClass {
                                        userType = SAPbobsCOM.BoMsgRcpTypes.rt_ContactPerson,
                                        userName = cardCode,
                                        userMail = contactMail });
                            }

                            recordSet.MoveNext();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                contactMail = null;
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }

            return contactDataList;
        }


        private static UserMailClass GetUsertMail(SAPbobsCOM.Company oCompany, string userCode)
        {
            UserMailClass userMail = null;
            SAPbobsCOM.Recordset recordSet;
            string sql;
            string tempMail;

            try
            {
                //Initializing objects
                recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                sql = string.Format(SAPSignaturesMailQuerys.GetUserMail, userCode);
                
                if (!string.IsNullOrWhiteSpace(sql))
                {
                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        recordSet.MoveFirst();
                        if (!recordSet.EoF)
                        {
                            tempMail = recordSet.Fields.Item("E_Mail").Value;

                            userMail = new UserMailClass { userType = SAPbobsCOM.BoMsgRcpTypes.rt_InternalUser, userName = userCode, userMail = tempMail };
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                userMail = null;
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }

            return userMail;
        }



        private static List<UserMailClass> GetMailListFromUserPerf(SAPbobsCOM.Company oCompany, string profileList)
        {
            List<UserMailClass> mailList = new List<UserMailClass>();
            SAPbobsCOM.Recordset recordSet;
            string sql;
            string uCodeString;
            string mailString;

            try
            {
                //Initializing objects
                recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                //With this sql we will know the UserListProfile to use, and if app user and customer will receive the mail
                sql = string.Format(SAPSignaturesMailQuerys.GetMailList_UserPerfilList, profileList);

                if (!string.IsNullOrWhiteSpace(sql))
                {
                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        recordSet.MoveFirst();
                        while (!recordSet.EoF)
                        {
                            uCodeString = recordSet.Fields.Item("U_DOI_UsrCode").Value;
                            mailString = recordSet.Fields.Item("E_Mail").Value;

                            if (!string.IsNullOrWhiteSpace(mailString))
                            {
                                if (!mailList.Any(x => x.userName == uCodeString))
                                    mailList.Add(new UserMailClass { userType = SAPbobsCOM.BoMsgRcpTypes.rt_InternalUser, userName = uCodeString, userMail = mailString });
                            }

                            recordSet.MoveNext();
                        }
                    }
                }
            } catch (Exception ex)
            {
                mailList = null;
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }
            //mailList = tempString.Split(';').Select(s => s.Trim()).ToList();    //split and trim

            return mailList;
        }

        private static List<UserMailClass> GetMailListForDocument (SAPbobsCOM.Company oCompany, int docType, string docEntry, string userCode)
        {
            List<UserMailClass> mailList = new List<UserMailClass>();
            SAPbobsCOM.Recordset recordSet;
            string sql;
            bool tempBool;

            List<UserMailClass> tempList;
            UserMailClass tempMail;

            try
            {
                //Initializing objects
                recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                //With this sql we will know the UserListProfile to use, and if app user and customer will receive the mail
                sql = string.Format(SAPSignaturesMailQuerys.GetMailList_ReceiverTypes, docType);

                if (!string.IsNullOrWhiteSpace(sql))
                {
                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        recordSet.MoveFirst();
                        while (!recordSet.EoF)
                        {
                            //Getting mail list from user profile
                            tempList = GetMailListFromUserPerf(oCompany, recordSet.Fields.Item("U_MailList").Value);
                            if (tempList != null)
                                mailList = tempList;

                            //Getting BP contact mail (if asked)
                            tempBool = recordSet.Fields.Item("U_SendToBP").Value == "Y";
                            if (tempBool)
                            {
                                tempList = GetBPContactMail(oCompany, docType, docEntry);
                                if ((tempList != null) && (tempList.Count > 0))
                                {
                                    /*if (!mailList.Any(x => x.userName == tempMail.userName && x.userType == tempMail.userType))
                                        mailList.Add(tempMail);*/
                                    mailList.AddRange(tempList);
                                }
                            }

                            //Getting User mail (if asked)
                            tempBool = recordSet.Fields.Item("U_SendToUser").Value == "Y";
                            if (tempBool)
                            {
                                tempMail = GetUsertMail(oCompany, userCode);
                                {
                                    if (!mailList.Any(x => x.userName == tempMail.userName && x.userType == tempMail.userType))
                                        mailList.Add(tempMail);
                                }
                            }

                            recordSet.MoveNext();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                mailList = null;
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }


            return mailList;
        }
        #endregion *****************************************************************************************************************


        #region Sending mails ******************************************************************************************************
        private static string GetDefaultSubjectMessageForDocType (SAPbobsCOM.Company oCompany, int docType, out string subject, out string message)
        {
            SAPbobsCOM.Recordset recordSet;
            string sql;
            string signaturePath = "";
            
            try
            {
                //Initializing objects
                subject = "";
                message = "";
                recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                sql = string.Format(SAPSignaturesMailQuerys.GetSubjectContentOfMail, docType);

                if (!string.IsNullOrWhiteSpace(sql))
                {
                    recordSet.DoQuery(sql);
                    ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                    if (recordSet.RecordCount > 0)
                    {
                        recordSet.MoveFirst();
                        while (!recordSet.EoF)
                        {
                            subject = recordSet.Fields.Item("U_Subject").Value;
                            message = recordSet.Fields.Item("U_Content").Value;
                            signaturePath = recordSet.Fields.Item("U_SignImg").Value;

                            message = message + "<br><br>" + GetImageAttachedHTML(signaturePath);

                            recordSet.MoveNext();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                subject = "";
                message = "";
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }

            return signaturePath;
        }


        private static void GetCardcodeAttachment(SAPbobsCOM.Company oCompany, int docType, string docEntry, out string cardCode, out string attachmentPath)
        {
            int iDocEntry;
            int idAttachment = -1;
            SAPbobsCOM.Attachments2 oAttachment;
            SAPbobsCOM.CompanyService companyService;
            SAPbobsCOM.PathAdmin pathAdmin;

            cardCode = "";
            attachmentPath = "";

            if (int.TryParse(docEntry, out iDocEntry))
            {
                switch (docType)
                {
                    case (int)DocumentTypeModel.DocumentTypeEnum.Albaran:
                        SAPbobsCOM.Documents delivery = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDeliveryNotes);
                        if (delivery.GetByKey(iDocEntry))
                        {
                            cardCode = delivery.CardCode;
                            idAttachment = delivery.AttachmentEntry;
                        }
                        break;
                    case (int)DocumentTypeModel.DocumentTypeEnum.Traslados:
                        SAPbobsCOM.IStockTransfer transfer = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oStockTransfer);
                        if (transfer.GetByKey(iDocEntry))
                        {
                            cardCode = transfer.CardCode;
                            idAttachment = transfer.AttachmentEntry;
                        }
                        break;
                    default:
                        break;
                }
            }

            if (idAttachment > -1)
            {
                oAttachment = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oAttachments2);
                if (oAttachment.GetByKey(idAttachment))
                {
                    companyService = oCompany.GetCompanyService();
                    pathAdmin = companyService.GetPathAdmin();
                    attachmentPath = pathAdmin.AttachmentsFolderPath + oAttachment.Lines.FileName + "." + oAttachment.Lines.FileExtension;
                    //attachmentPath = oAttachment.Lines.SourcePath + "/" + oAttachment.Lines.FileName + "." + oAttachment.Lines.FileExtension;
                }
            }
        }


        private static string SendMailToList (SAPbobsCOM.Company oCompany, List<UserMailClass> mailList, int docType, string docEntry, string userCode)
        {
            SAPbobsCOM.Messages objMsg;
            string subject;
            string message;
            string cardCode;
            string attachmentPath;
            int iError;
            string _error = "";

            try
            {
                if ((mailList != null) && (mailList.Count > 0))
                {
                    objMsg = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oMessages);
                    GetDefaultSubjectMessageForDocType(oCompany, docType, out subject, out message);
                    GetCardcodeAttachment(oCompany, docType, docEntry, out cardCode, out attachmentPath);

                    //Main mail data
                    objMsg.Subject = subject;
                    objMsg.MessageText = message;
                    //Receivers of the mail
                    for (int i = 0; i < mailList.Count; i++)
                    //for (int i = mailList.Count - 1; i >= 0 /*mailList.Count - 2*/; i--)
                    {
                        objMsg.Recipients.Add();
                        objMsg.Recipients.SetCurrentLine(i);
                        objMsg.Recipients.SendInternal = SAPbobsCOM.BoYesNoEnum.tNO;
                        objMsg.Recipients.SendEmail = SAPbobsCOM.BoYesNoEnum.tYES;
                        /*switch (mailList[i].Item1)
                        {
                            case SAPbobsCOM.BoMsgRcpTypes.rt_InternalUser:
                                //objMsg.Recipients.UserCode = userCode;
                                switch (i)
                                {
                                    case 0: objMsg.Recipients.UserCode = "Andres"; break;
                                    case 1: objMsg.Recipients.UserCode = "Tamara"; break;
                                    case 3: objMsg.Recipients.UserCode = "manager"; break;
                                }
                                break;
                            case SAPbobsCOM.BoMsgRcpTypes.rt_ContactPerson:
                                objMsg.Recipients.UserCode = cardCode;
                                break;
                        }*/
                        objMsg.Recipients.UserCode = mailList[i].userName;// userCode;
                        objMsg.Recipients.UserType = mailList[i].userType;
                        objMsg.Recipients.EmailAddress = mailList[i].userMail;
                    }
                    //Attachment (PDF document)
                    objMsg.Attachments.Add();
                    objMsg.Attachments.Item(0).FileName = attachmentPath;

                    //Sending
                    objMsg.Add();
                    oCompany.GetLastError(out iError, out _error);
                    if (iError != 0)
                    {
                        ErrorHandler.WriteInLog("Attaching file to mail. Error " + iError + ": " + _error, ErrorHandler.ErrorVerboseLevel.None);
                    }

                }
            } catch (Exception ex)
            {
                _error = "EXCEPTION | " + ex.Message;
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }

            return _error;
        }


        private static string SendMailToUser(SAPbobsCOM.Company oCompany, UserMailClass mailUser, int docType, string docEntry, string userCode)
        {
            SAPbobsCOM.Messages objMsg;
            string subject;
            string message;
            string cardCode;
            string attachmentPath;
            List<string> listAttachments;
            int iError;
            string _error = "";

            string tempString;

            try
            {
                if (mailUser != null)
                {
                    objMsg = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oMessages);
                    tempString = GetDefaultSubjectMessageForDocType(oCompany, docType, out subject, out message);
                    GetCardcodeAttachment(oCompany, docType, docEntry, out cardCode, out attachmentPath);

                    //Main mail data
                    objMsg.Subject = subject;
                    objMsg.MessageText = message;
                    //Receivers of the mail
                    
                    objMsg.Recipients.Add();
                    //objMsg.Recipients.SetCurrentLine(i);
                    objMsg.Recipients.SendInternal = SAPbobsCOM.BoYesNoEnum.tNO;
                    objMsg.Recipients.SendEmail = SAPbobsCOM.BoYesNoEnum.tYES;
                        
                    objMsg.Recipients.UserCode = mailUser.userName;// userCode;
                    objMsg.Recipients.UserType = mailUser.userType;
                    objMsg.Recipients.EmailAddress = mailUser.userMail;

                    /***********************************************************************************
                    //Attachment (PDF document)
                    objMsg.Attachments.Add();
                    objMsg.Attachments.Item(0).FileName = attachmentPath;
                    //Attachment (signature file)
                    if (!string.IsNullOrWhiteSpace(tempString))
                    {
                        objMsg.Attachments.Add();
                        objMsg.Attachments.Item(1).FileName = tempString;
                    }
                    //***********************************************************************************/
                    listAttachments = new List<string>();
                    listAttachments.Add(attachmentPath);
                    if (!string.IsNullOrWhiteSpace(tempString))
                        listAttachments.Add(tempString);

                    objMsg.AttachmentEntry = SAPAttachments.SAPAttachments.AddAttachments(oCompany, listAttachments);


                    //Sending
                    objMsg.Add();
                    oCompany.GetLastError(out iError, out _error);
                    if (iError != 0)
                    {
                        ErrorHandler.WriteInLog("Attaching file to mail. Error " + iError + ": " + _error, ErrorHandler.ErrorVerboseLevel.None);
                    }

                }
            }
            catch (Exception ex)
            {
                _error = "EXCEPTION | " + ex.Message;
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }

            return _error;
        }
        #endregion *****************************************************************************************************************


        public static string SendMailForDocument (SAPbobsCOM.Company oCompany, DocumentSignatureSendModel signature)
        {
            List<UserMailClass> mailList;
            string _error = "";

            mailList = new List<UserMailClass>();
            if (signature.sendByMail)
            {
                mailList = GetMailListForDocument(oCompany, signature.docType, signature.docEntry, signature.userCode);

                if ((mailList != null) && (mailList.Count > 0))
                {
                    _error = "Se han obtenido los siguientes correos:";
                    foreach (UserMailClass correo in mailList)
                    {
                        _error += Environment.NewLine + "  - (" + correo.userType.ToString() + ") " + correo.userName + " <" + correo.userMail + ">";
                    }
                    ErrorHandler.WriteInLog(_error, ErrorHandler.ErrorVerboseLevel.Trace);
                    _error = "";

                    //_error = SendMailToList(oCompany, mailList, signature.docType, signature.docEntry, signature.userCode);
                    //For BP, all contacts share the same CardCode, so we can't send mails to more than one contact for BP, so we must send mails individually
                    for (int i = 0; i < mailList.Count; i++)
                        _error += SendMailToUser(oCompany, mailList[i], signature.docType, signature.docEntry, signature.userCode) + Environment.NewLine;
                }
            }

            //Notifing error
            if (!string.IsNullOrWhiteSpace(_error))
            {
                ErrorHandler.WriteInLog("Mail sending error " + _error.Replace("|", ": "), ErrorHandler.ErrorVerboseLevel.None);
                _error = _error.Replace("|", ": ");
            }

            return _error;
        }


        #region private functions ***************************************************************************************************************
        /// <summary>
        /// Gets an HTML with the file bytecode embedded on the code. This way you can send a file without attaching or linking on net, but some mail servers
        /// wont load it because security reasons
        /// </summary>
        /// <param name="_path"></param>
        /// <returns></returns>
        private static string GetImageEmbeddedHTML(string _path)
        {
            byte[] image;
            string extension;
            string htmlCode = "";

            if (!string.IsNullOrWhiteSpace(_path))
            {
                try
                {

                    image = System.IO.File.ReadAllBytes(_path);

                    extension = System.IO.Path.GetExtension(_path);
                    if ((extension.ToLower() == ".jpg") || (extension.ToLower() == ".jpeg"))
                    {
                        htmlCode = string.Format("data:image/jpg;base64,{0}", Convert.ToBase64String(image));
                    }
                    else if ((extension.ToLower() == ".png"))
                    {
                        htmlCode = string.Format("data:image/png;base64,{0}", Convert.ToBase64String(image));
                    }

                    if (!string.IsNullOrWhiteSpace(htmlCode))
                        htmlCode = string.Format("<img src=\"{0}\">", htmlCode);

                    if (htmlCode.Length > 62000)
                        htmlCode = "- Image too big. -";

                } catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return htmlCode;
        }

        /// <summary>
        /// Add the HTML code to view the image linking into an attached image. SAP uses the filename with extension as CID for the attached file.
        /// This function DOES NOT attach the file.
        /// NOTE: some web server might ask to download attached files.
        /// </summary>
        /// <param name="_path"></param>
        /// <returns></returns>
        private static string GetImageAttachedHTML(string _path)
        {
            string htmlCode = "";

            if (!string.IsNullOrWhiteSpace(_path))
            {
                try
                {
                    htmlCode = "cid:" + System.IO.Path.GetFileName(_path);

                    if (!string.IsNullOrWhiteSpace(htmlCode))
                        htmlCode = string.Format("<img src=\"{0}\">", htmlCode);

                    if (htmlCode.Length > 62000)
                        htmlCode = "- Image too big. -";

                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return htmlCode;
        }
        #endregion*******************************************************************************************************************************
    }
}