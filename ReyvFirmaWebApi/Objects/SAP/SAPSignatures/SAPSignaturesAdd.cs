﻿using ReyvFirmaWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Objects.SAP.SAPSignatures
{
    public static class SAPSignaturesAdd
    {
        #region Data check ***********************************************
        private static int CheckData(DocumentSignatureSendModel data)
        {
            int i;

            if (data == null)
                return 1;
            if ((data.docType == 0) || (string.IsNullOrWhiteSpace(data.docEntry)))
                return 2;
            if (!int.TryParse(data.docEntry, out i))
                return 3;
            /*if (data.signature == null)
                return 4;*/

            return 0;
        }
        #endregion *******************************************************


        public static string GetSignatureID(SAPbobsCOM.Company oCompany, int docType, string docEntry)
        {
            SAPbobsCOM.Recordset recordSet;
            string sql;
            string signID = "";

            if ((oCompany != null) && (oCompany.Connected))
            {
                try
                {
                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

                    sql = string.Format(SAPSignaturesQuerys.GetSignatureCode, docType, docEntry);

                    if (!string.IsNullOrWhiteSpace(sql))
                    {
                        recordSet.DoQuery(sql);
                        ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                        if (!recordSet.EoF)
                        {
                            recordSet.MoveFirst();
                            signID = recordSet.Fields.Item("Code").Value.ToString();
                        }

                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return signID;
        }


        public static bool AddSignature(SAPbobsCOM.Company oCompany, DocumentSignatureSendModel signatureData)
        {
            SAPbobsCOM.CompanyService oCompService;
            SAPbobsCOM.GeneralService oGeneralService;
            SAPbobsCOM.GeneralData oGeneralData;

            int dataError;
            string newCode;
            bool saved = false;
            string _error;
            int iError;

            dataError = CheckData(signatureData);
            if (dataError == 0)
            {
                try
                {
                    oCompService = oCompany.GetCompanyService();
                    oGeneralService = oCompService.GetGeneralService("DOI_FIRMAS");
                    oGeneralData = (SAPbobsCOM.GeneralData)oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);

                    newCode = SAPGenerics.UDOGetNextNumericStringCode(oCompany, "@DOI_FIRMAS", "Code", "00000000");
                    oGeneralData.SetProperty("Code", newCode);
                    oGeneralData.SetProperty("U_DocType", signatureData.docType);
                    oGeneralData.SetProperty("U_DocEntry", int.Parse(signatureData.docEntry));
                    //oGeneralData.SetProperty("U_SignDate", DateTime.Now);
                    //oGeneralData.SetProperty("U_SignTime", DateTime.Now);

                    //We are not saving the signature image, but if we want to do it, look for LoadBlobFrom File in SAP SDK
                    //System.IO.Stream tempImage = new System.IO.MemoryStream(signatureData.signature);
                    //oGeneralData.SetProperty("U_SignImg", tempImage);

                    oGeneralService.Add(oGeneralData);
                    oGeneralService = null;
                    oGeneralData = null;

                    oCompService = null;
                    oCompany.GetLastError(out iError, out _error);
                    if (iError != 0)
                    {
                        ErrorHandler.WriteInLog("Adding signature info. Error " + iError + ": " + _error, ErrorHandler.ErrorVerboseLevel.None);
                        saved = false;
                    }
                    else
                    {
                        saved = true;
                    }
                }
                catch (Exception ex)
                {
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                    saved = false;
                }
            }
            else
            {
                ErrorHandler.WriteInLog(string.Format("ERROR saving signature ({0})", dataError), ErrorHandler.ErrorVerboseLevel.None);
                saved = false;
            }

            return saved;
        }

        public static bool UpdateSignature(SAPbobsCOM.Company oCompany, DocumentSignatureSendModel signatureData)
        {
            //SAPbobsCOM.UserTable uTable = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables);
            SAPbobsCOM.UserTable uTable = oCompany.UserTables.Item("DOI_FIRMAS");
            string _error;
            int iError;
            bool saved = false;
            string signCode = GetSignatureID(oCompany, signatureData.docType, signatureData.docEntry);

            if (!string.IsNullOrWhiteSpace(signCode))
            {
                uTable.GetByKey(signCode);
                uTable.UserFields.Fields.Item("U_SignDate").Value = DateTime.Now;
                uTable.UserFields.Fields.Item("U_SignTime").Value = DateTime.Now;
                uTable.UserFields.Fields.Item("U_Signed").Value = "Y";
                //uTable.UserFields.Fields.Item("U_SignImg").Value = signatureData.signature;

                uTable.Update();
                oCompany.GetLastError(out iError, out _error);
                if (iError == 0)
                    saved = true;
                else
                {
                    ErrorHandler.WriteInLog("Adding signature info. Error " + iError + ": " + _error, ErrorHandler.ErrorVerboseLevel.None);
                    saved = false;
                }
            }

            return saved;
        }


        public static void DEBUGCreateSingatureListElements(SAPbobsCOM.Company oCompany)
        {
            SAPbobsCOM.CompanyService oCompService;
            SAPbobsCOM.GeneralService oGeneralService;
            SAPbobsCOM.GeneralData oGeneralData;

            string newCode;
            string _error;
            int iError;

            try
            {
                oCompService = oCompany.GetCompanyService();
                oGeneralService = oCompService.GetGeneralService("DOI_FIRMAS");
                oGeneralData = (SAPbobsCOM.GeneralData)oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralData);

                for (int i = 0; i < 10; i++)
                {
                    newCode = SAPGenerics.UDOGetNextNumericStringCode(oCompany, "@DOI_FIRMAS", "Code", "00000000");
                    oGeneralData.SetProperty("Code", newCode);
                    oGeneralData.SetProperty("U_DocType", (int)DocumentTypeModel.DocumentTypeEnum.Albaran);
                    oGeneralData.SetProperty("U_DocEntry", i);
                    oGeneralData.SetProperty("U_Signed", "N");

                    oGeneralService.Add(oGeneralData);
                    oCompany.GetLastError(out iError, out _error);
                    if (iError != 0)
                    {
                        ErrorHandler.WriteInLog("Adding signature info. Error " + iError + ": " + _error, ErrorHandler.ErrorVerboseLevel.None);
                    }
                }

                for (int i = 0; i < 10; i++)
                {
                    newCode = SAPGenerics.UDOGetNextNumericStringCode(oCompany, "@DOI_FIRMAS", "Code", "00000000");
                    oGeneralData.SetProperty("Code", newCode);
                    oGeneralData.SetProperty("U_DocType", (int)DocumentTypeModel.DocumentTypeEnum.Traslados);
                    oGeneralData.SetProperty("U_DocEntry", i + 1);
                    oGeneralData.SetProperty("U_Signed", "N");

                    oGeneralService.Add(oGeneralData);
                    oCompany.GetLastError(out iError, out _error);
                    if (iError != 0)
                    {
                        ErrorHandler.WriteInLog("Adding signature info. Error " + iError + ": " + _error, ErrorHandler.ErrorVerboseLevel.None);
                    }
                }

                oGeneralService = null;
                oGeneralData = null;

                oCompService = null;

            }
            catch (Exception ex)
            {
                ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
            }
        }
    }
}