﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Objects.SAP.SAPPDF
{
    public static class SAPPDFData
    {
        /// <summary>
        /// Get a complete list of deliveries
        /// </summary>
        /// <param name="oCompany"></param>
        /// <param name="userCode"></param>
        /// <returns></returns>
        public static string GetPDFDocumentURL(SAPbobsCOM.Company oCompany, string sql)
        {
            SAPbobsCOM.Recordset recordSet;
            string documentURL = null;

            if ((oCompany != null) && (oCompany.Connected))
            {
                try
                {
                    #region DEBUG ***********************************************************************************************************************************************
                    //documentURL = sql;
                    #endregion **************************************************************************************************************************************************

                    //Initializing objects
                    recordSet = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    

                    if (!string.IsNullOrWhiteSpace(sql))
                    {
                        recordSet.DoQuery(sql);
                        ErrorHandler.WriteInLog("Total records found: " + recordSet.RecordCount.ToString(), ErrorHandler.ErrorVerboseLevel.Trace);
                        if (recordSet.RecordCount > 0)
                        {
                            recordSet.MoveFirst();
                            if (!recordSet.EoF)
                            {
                                documentURL = recordSet.Fields.Item("U_DOI_PDF").Value.ToString();
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    documentURL = null;
                    ErrorHandler.WriteInLog("EXCEPTION! " + ex.Message + Environment.NewLine + ex.StackTrace, ErrorHandler.ErrorVerboseLevel.None);
                }
            }

            return documentURL;
        }

        public static string GetSecuritySheetURL(SAPbobsCOM.Company oCompany)
        {
            return GetPDFDocumentURL(oCompany, SAPPDFQuerys.GetSecuritySheetURL);
            //return GetPDFDocumentURL(oCompany, /*"http://192.168.1.127:59214/" +*/ "informes/1v3.pdf");
        }

        public static string GetLOPDURL(SAPbobsCOM.Company oCompany)
        {
            //return GetPDFDocumentURL(oCompany, SAPPDFQuerys.GetLOPDURL);
            return GetPDFDocumentURL(oCompany, /*"http://192.168.1.127:59214/" +*/ "informes/LOPD.pdf");
        }

    }
}