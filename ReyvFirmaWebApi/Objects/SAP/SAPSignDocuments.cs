﻿using ReyvFirmaWebApi.Models;
using ReyvFirmaWebApi.Objects.SAP.SAPDeliveries;
using ReyvFirmaWebApi.Objects.SAP.SAPEpis;
using ReyvFirmaWebApi.Objects.SAP.SAPTransfers;
using ReyvFirmaWebApi.Objects.SAP.SAPVisitors;
using System.Collections.Generic;

namespace ReyvFirmaWebApi.Objects.SAP
{
    public static class SAPSignDocuments
    {
        public static List<DocumentDataModel> GetDocumentToSignList (SAPbobsCOM.Company oCompany, string userCode, int docType)
        {
            List<DocumentDataModel> docList = null;

            switch (docType)
            {
                case (int)DocumentTypeModel.DocumentTypeEnum.Albaran:
                    docList = SAPDeliveriesData.GetDeliveriesList(oCompany, userCode);
                    break;
                case (int)DocumentTypeModel.DocumentTypeEnum.Traslados:
                    docList = SAPInventoryTransfers.GetInventoryTransferList(oCompany, userCode);
                    break;
                case (int)DocumentTypeModel.DocumentTypeEnum.Visitas:
                    docList = SAPVisitorsData.GetVisitorsList(oCompany, userCode);
                    break;
                case (int)DocumentTypeModel.DocumentTypeEnum.Epis:
                    docList = SAPEpisData.GetEpisList(oCompany, userCode);
                    break;
            }

            return docList;
        }

        public static List<DocumentLineDataModel> GetDocumentLines (SAPbobsCOM.Company oCompany, string docEntry, int docType)
        {
            List<DocumentLineDataModel> docLines = null;

            switch (docType)
            {
                case (int)DocumentTypeModel.DocumentTypeEnum.Epis:
                    docLines = SAPEpisData.GetEpisLinesList(oCompany, docEntry);
                    break;
            }

            return docLines;
        }
    }
}