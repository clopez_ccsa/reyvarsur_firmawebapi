﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Objects
{
    public class CompanyConnection
    {
        private readonly static double segundosParaLogOff =
            Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings.Get("CompanyConnectionTimeOut")); //6000;

        SAPbobsCOM.Company oCompany;    //object to comunicate with SAP DI-API
        DateTime lastConnection; //Date of the last connection
        bool myTimer;   //Indicates if this is subscribed to a ElapsedEventHandler

        // CONSTRUCTORS **************************************************************************
        /// <summary>
        /// Basic constructor
        /// </summary>
        public CompanyConnection()
        {
            oCompany = new SAPbobsCOM.Company();
            lastConnection = new DateTime();
        }


        /// <summary>
        /// Constructor. Creates a CompanyConection and initializes the variables, but it don't connect
        /// to SAP DI-API (you must use CompanyConection.Connect())
        /// </summary>
        /// <param name="_server"></param>
        /// <param name="_companyDB"></param>
        /// <param name="_username"></param>
        /// <param name="_password"></param>
        /// <param name="_dbServerType"></param>
        public CompanyConnection(string _licenseServer, string _server, string _companyDB, string _username,
                                    string _password, SAPbobsCOM.BoDataServerTypes _dbServerType)
        {
            oCompany = new SAPbobsCOM.Company();
            lastConnection = new DateTime();

            oCompany.LicenseServer = _licenseServer;
            oCompany.Server = _server;
            oCompany.CompanyDB = _companyDB;
            oCompany.UserName = _username;
            oCompany.Password = _password;
            oCompany.DbServerType = _dbServerType;
        }
        
        //******************************************************************************************

        // GETTERS AND SETTERS *********************************************************************
        /// <summary>
        /// Get the SAPbobsCOM.Company object to make a SAP operation
        /// </summary>
        /// <param name="updateDate">Indicates if a date update must be done with this request, to avoid no activity disconection</param>
        /// <returns>The SAPbobsCOM.Company object to make a SAP operation</returns>
        public SAPbobsCOM.Company GetCompany (bool updateDate)
        {
            if (oCompany != null)
                if (updateDate)
                {
                    lastConnection = DateTime.Now;
                    System.Diagnostics.Debug.WriteLine("*** UPDATE (getCompany) ***");
                }
            return oCompany;
        }

        /// <summary>
        /// Set the values for the SAPbobsCOM.Company object
        /// </summary>
        /// <param name="_server"></param>
        /// <param name="_companyDB"></param>
        /// <param name="_username"></param>
        /// <param name="_password"></param>
        /// <param name="_dbServerType"></param>
        public void SetCompany(string _server, string _companyDB, string _username, string _password,
                        SAPbobsCOM.BoDataServerTypes _dbServerType)
        {
            oCompany.Server = _server;
            oCompany.CompanyDB = _companyDB;
            oCompany.UserName = _username;
            oCompany.Password = _password;
            oCompany.DbServerType = _dbServerType;
        }
        //********************************************************************************************


        /// <summary>
        /// Tries to connect to SAP DI-API with the data contained on his Company object variable
        /// </summary>
        /// <returns>0 if there is no error, or the error code from DI-API</returns>
        public int Connect ()
        {

            int _error;
            List<string> cData = new List<string>();
            cData.Add("Conectando!!");
            cData.Add("Server: " + oCompany.Server);
            cData.Add("Company DB: " + oCompany.CompanyDB);
            cData.Add("User Name: " + oCompany.UserName);
            cData.Add("Password: " + oCompany.Password);
            cData.Add("DB Server type: " + oCompany.DbServerType.ToString());
            ErrorHandler.WriteInLog(cData, ErrorHandler.ErrorVerboseLevel.None);


            _error = oCompany.Connect();
            if (_error == 0)
            {
                lastConnection = DateTime.Now;
                System.Diagnostics.Debug.WriteLine("*** UPDATE (conexion) ***");
            }
            return _error;
        }

        /// <summary>
        /// Disconnect from SAP DI-API
        /// </summary>
        public void Disconnect()
        {
            //I can delete myself from the connection list because I don't know my own key in connection list dictionary
            //So just disconnect, and then ConnectionsManager will dispose me with a new connection with the same key.
            ErrorHandler.WriteInLog(string.Format("Connection closed!! ({0})", oCompany.UserName), ErrorHandler.ErrorVerboseLevel.None);
            DeleteFromTimeoutEvents();
            while (oCompany.Connected)
                oCompany.Disconnect();
        }

        /// <summary>
        /// Check if this object is connected to SAP DI-API
        /// </summary>
        /// <returns>True if it's connected</returns>
        public bool IsConnected()
        {
            return oCompany.Connected;
        }

        // EVENT HANDLER ***********************************************************************
        /// <summary>
        /// Add this object to a Timer event list, to listen it's time interruptions
        /// </summary>
        /// <param name="timeoutTimer">The Timer to listen to</param>
        public void AddToTimeoutEvents ()
        {
            DeleteFromTimeoutEvents();
            TimeoutTimer.get().Elapsed += TickConnection;
            myTimer = true;
        }

        /// <summary>
        /// Delete this object from it's Timer event list
        /// </summary>
        private void DeleteFromTimeoutEvents ()
        {
            if (myTimer)
                TimeoutTimer.get().Elapsed -= TickConnection;

            myTimer = false;
        }

        /// <summary>
        /// Action to make when Timer raises an event. It's only neccesary for the timer, not the user.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        public void TickConnection (object source, System.Timers.ElapsedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Han pasado 3 segundos según el timer.");
            if (DateTime.Now > lastConnection.AddSeconds(segundosParaLogOff))
            {
                System.Diagnostics.Debug.WriteLine("Desconectado!!");
                ErrorHandler.WriteInLog("Company connection timed out!!", ErrorHandler.ErrorVerboseLevel.None);
                this.Disconnect();
                lastConnection = DateTime.Now;
            }
        }
    }
}