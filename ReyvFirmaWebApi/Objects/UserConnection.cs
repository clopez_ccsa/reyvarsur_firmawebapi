﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReyvFirmaWebApi.Objects
{
    public class UserConnection
    {
        private readonly static double segundosParaLogOff = 
            Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings.Get("UserConnectionTimeOut")); //6000;

        public string username { set; get; }
        public string password { private set; get; }
        public string token { set; get; }
        public string connectionGroup { get;  set; }
        private DateTime lastConnection;
        private bool myTimer;

        // CONSTRUCTORS *******************************************************************************
        /// <summary>
        /// Creates a new node based on user name and password
        /// </summary>
        /// <param name="_username"></param>
        /// <param name="_password"></param>
        /// <param name="_token">Conection identifier</param>
        public UserConnection(string _username, string _password, string _token)
        {
            username = _username;
            password = _password;
            token = _token;
            lastConnection = DateTime.Now;
        }

        // METHODS ************************************************************************************
        /// <summary>
        /// Update the time of the last conection to NOW
        /// </summary>
        public void UpdateDateTime ()
        {
            System.Diagnostics.Debug.WriteLine("USER: Fecha actualizada.");
            lastConnection = DateTime.Now;
        }

        // EVENTS *************************************************************************************
        
        /// <summary>
        /// Add this object to a Timer event list, to listen it's time interruptions
        /// </summary>
        /// <param name="timeoutTimer">The Timer to listen to</param>
        public void AddToTimeoutEvents()
        {
            DeleteFromTimeoutEvents();
            TimeoutTimer.get().Elapsed += TickConnection;
            myTimer = true;
        }

        /// <summary>
        /// Delete this object from it's Timer event list
        /// </summary>
        public void DeleteFromTimeoutEvents()
        {
            if (myTimer)
                TimeoutTimer.get().Elapsed -= TickConnection;

            myTimer = false;
        }

        /// <summary>
        /// Action to make when Timer raises an event. It's only neccesary for the timer, not the user.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        public void TickConnection (object source, System.Timers.ElapsedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("USER (" + token +
                "): Han pasado 3 segundos según el timer.");

            if (DateTime.Now > lastConnection.AddSeconds(segundosParaLogOff))
            {
                System.Diagnostics.Debug.WriteLine("User kicked out!!");
                ErrorHandler.WriteInLog(string.Format("User {0} timed out!!", username), ErrorHandler.ErrorVerboseLevel.None);
                lastConnection = DateTime.Now;
                DeleteFromTimeoutEvents();
                ConnectionsManager.DeleteUser(this);
            }
        }
        
    }
}